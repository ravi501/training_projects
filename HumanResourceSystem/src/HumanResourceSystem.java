import hrpkg.HR_GUI;
import hrpkg.PersonSummary;

import java.util.ArrayList;
import java.util.HashMap;


public class HumanResourceSystem
{
    // Human Resource System handles the processing of client benefits

    // Singleton instance for HumanResourceSystem class
    private static HumanResourceSystem instance;

    // Array List to store the list of employees
    private ArrayList< Employee >      employeeArray       = new ArrayList< Employee >();

    // HashMap to store the employee type corresponding to the employee number
    private HashMap< String, String >  employeeTypeMap     = new HashMap< String, String >();

    // PersonSummary list to send it to the GUI class
    private ArrayList< PersonSummary > personList          = new ArrayList< PersonSummary >();

    // Index for accessing the employee is and employee type
    final static int                   EMPLOYEE_ID_INDEX   = 0;
    final static int                   EMPLOYEE_TYPE_INDEX = 1;

    final static int                   EMPLOYEE_INDEX_DATA = 2;

    /**
     * Getter method for employee array
     * 
     * @return
     */
    public ArrayList< Employee > getEmployeeArray()
    {
        return employeeArray;
    }

    /**
     * Setter method for employee array
     * 
     * @param employeeArray
     */
    public void setEmployeeArray( ArrayList< Employee > employeeArray )
    {
        this.employeeArray = employeeArray;
    }

    /**
     * Getter method for Hash map employeeType array
     * 
     * @return
     */
    public HashMap< String, String > getEmployeeTypeArray()
    {
        return employeeTypeMap;
    }

    /**
     * Setter method for employeeType array
     * 
     * @param employeeTypeArray
     */
    public void setEmployeeTypeArray(
            HashMap< String, String > employeeTypeArray )
    {
        this.employeeTypeMap = employeeTypeArray;
    }

    /**
     * Getter method for personList
     * 
     * @return
     */
    public ArrayList< PersonSummary > getPersonList()
    {
        return personList;
    }

    /**
     * Setter method for personList
     * 
     * @param personList
     */
    public void setPersonList( ArrayList< PersonSummary > personList )
    {
        this.personList = personList;
    }

    private HumanResourceSystem()
    {

    }

    /**
     * Gets a singleton instance of the Human Resource System class
     * 
     * @return
     */
    public static HumanResourceSystem getInstance()
    {
        if ( instance == null )
        {
            instance = new HumanResourceSystem();
        }

        return instance;
    }

    /**
     * Begins the processing of the Human Resource System class
     */
    public void process()
    {
        loadEmployeeType();
        loadEmployeeArray();
        loadPersonSummary();
        displayGUI();
    }

    /**
     * Loads the employee type Hash Map by populating the employee id and
     * employee type corresponding to the id
     */
    private void loadEmployeeType()
    {
        for ( int i = 0; i < EmployeeData.employeeTypeArray.length; i++ )
        {
            employeeTypeMap
                    .put( ( String ) EmployeeData.employeeTypeArray[ i ][ EMPLOYEE_ID_INDEX ],
                            ( String ) EmployeeData.employeeTypeArray[ i ][ EMPLOYEE_TYPE_INDEX ] );
        }
    }

    /**
     * Loads the employee array with the information of all the employees
     */
    private void loadEmployeeArray()
    {
        Employee employee;

        for ( int i = 0; i < EmployeeData.employeeAttributeArray.length; i++ )
        {
            EmployeeParser parser = new EmployeeParser(
                    EmployeeData.employeeAttributeArray[ i ] );

            employee = Employee
                    .getEmployeeType( getEmployeeTypeArray()
                            .get( EmployeeData.employeeAttributeArray[ i ][ EMPLOYEE_INDEX_DATA ] ) );

            if ( parser.getEmployeeFirstName() != null )
            {
                employee.build( parser );
                employeeArray.add( employee );
            }
        }
    }

    /**
     * Loads the person summary object and then adds to the array list of
     * persons to be used by the GUI
     */
    private void loadPersonSummary()
    {
        for ( int i = 0; i < employeeArray.size(); i++ )
        {
            PersonSummary person = new PersonSummary();
            Employee employee = employeeArray.get( i );

            person.setName( employee.getEmployeeFirstName() + " "
                    + employee.getEmployeeLastName() );
            person.setID( employee.getEmployeeID() );
            person.setAssocType( employee.getEmployeeType() );
            person.setHighlyComp( Utility.checkBenefitEligibility( employee
                    .getHighCompensation() ) );
            person.setHireDate( Utility.processHireDate( employee.getHireDate() ) );
            person.setLifeInsAmount( Utility.applyCurrencyFormat( employee
                    .calculateInsurance() ) );
            person.setRetireEligible( Utility.checkBenefitEligibility( employee
                    .getRetirementEligibility() ) );
            person.setSalary( Utility.applyCurrencyFormat( employee
                    .calculateEmployeeSalary() ) );
            person.setSickRem( Utility.applyDecimalFormat( employee
                    .calculateSickRemain() ) );
            person.setVacEarned( Utility.applyDecimalFormat( employee
                    .getVacationEarned() ) );
            person.setVacRem( Utility.applyDecimalFormat( employee
                    .calculateVacationRemain() ) );
            person.setVacTaken( Utility.applyDecimalFormat( employee
                    .getVacationTaken() ) );

            personList.add( person );
        }
    }

    /**
     * Method for displaying the GUI
     */
    private void displayGUI()
    {
        HR_GUI.getInstance( personList );
    }

    /**
     * Gets the type of the employee when an employee ID is entered
     * 
     * @param employeeID
     *            - ID of the currently referred employee
     * @return - Returns the type of the employee corresponding to the id
     */
    public String getEmployeeType( String employeeID )
    {
        String employeeType = employeeTypeMap.get( employeeID );

        return employeeType;
    }

}
