public class EmployeeParser
{
    private Object   employeeData[]         = null;

    final static int EMPLOYEE_FIRST_NAME    = 0;
    final static int EMPLOYEE_LAST_NAME     = 1;
    final static int EMPLOYEE_ID            = 2;
    final static int EMPLOYEE_HIRE_DATE     = 3;
    final static int EMPLOYEE_SALARY_PERIOD = 4;
    final static int SALARY_RATE_PERIOD     = 5;
    final static int LENGTH_OF_EMPLOYMENT   = 6;
    final static int SICK_TIME_EARNED       = 7;
    final static int SICK_TIME_TAKEN        = 8;
    final static int VACATION_EARNED        = 9;
    final static int VACATION_TAKEN         = 10;

    public EmployeeParser( Object line[] )
    {
        this.employeeData = line;
    }

    public String getEmployeeFirstName()
    {
        return ( String ) employeeData[ EMPLOYEE_FIRST_NAME ];
    }

    public String getEmployeeLastName()
    {
        return ( String ) employeeData[ EMPLOYEE_LAST_NAME ];
    }

    public String getEmployeeID()
    {
        return ( String ) employeeData[ EMPLOYEE_ID ];
    }

    public String getHireDate()
    {
        return ( String ) employeeData[ EMPLOYEE_HIRE_DATE ];
    }

    public Integer getSalaryPeriod()
    {
        return ( Integer ) employeeData[ EMPLOYEE_SALARY_PERIOD ];
    }

    public Double getSalaryRate()
    {
        return ( Double ) employeeData[ SALARY_RATE_PERIOD ];
    }

    public Double getEmploymentLength()
    {
        return ( Double ) employeeData[ LENGTH_OF_EMPLOYMENT ];
    }

    public Double getSickTimeEarned()
    {
        return ( Double ) employeeData[ SICK_TIME_EARNED ];
    }

    public Double getSickTimeTaken()
    {
        return ( Double ) employeeData[ SICK_TIME_TAKEN ];
    }

    public Double getVacationEarned()
    {
        return ( Double ) employeeData[ VACATION_EARNED ];
    }

    public Double getVacationTaken()
    {
        return ( Double ) employeeData[ VACATION_TAKEN ];
    }

}
