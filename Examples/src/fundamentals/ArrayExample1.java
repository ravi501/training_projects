
package fundamentals;

public class ArrayExample1
{

    public static void main( String[] args )
    {
        int[] array1 = new int[ 2 ]; // array declaration & allocation
        array1[ 0 ] = 100; // array initialization

        String[] array2; // array declaration
        array2 = new String[ 9 ]; // array allocation
        array2[ 99 ] = "oops!"; // will cause runtime exception

        // Array declaration & implicit allocation thru initialization
        int[] array3 = { 1, 2, 3 };

        // Multi-dimensional, jagged array
        String[][] array4 = { { "One" }, { "Two", "2" } };

        // Anonymous Array Creation
        doSomething( new int[] { 11, 22, 33, 44, 55 } );
    }

    static void doSomething( int[] array )
    {
        // do something
    }
}
