
package fundamentals;

class OuterClass1
{

    private static int classVariable = 1;

    private static void classMethod()
    {
        System.out.println( "OuterClass1       -> classMethod()" );
    }

    private int instanceVariable = 2;

    private void instanceMethod()
    {
        System.out.println( "OuterClass1       -> instanceMethod()" );
    }

    // Has access to *ALL STATIC* members of Outer class
    public static class StaticNestedClass
    {

        public static void nestedClassClassMethod()
        {
            System.out.println( "StaticNestedClass -> classMethod()" );
            classVariable = 0;
            classMethod();

            // The following code will not compile
            // instanceVariable = 0;
            // instanceMethod();
        }

        public void nestedClassInstanceMethod()
        {
            System.out.println( "StaticNestedClass -> instanceMethod()" );

            // The following code will not compile
            // instanceVariable = 0;
            // instanceMethod();
        }
    }
}


public class StaticNestedClassExample1
{

    public static void main( String[] args )
    {
        // Call a static method on the nested class
        OuterClass1.StaticNestedClass.nestedClassClassMethod();

        // Create an instance of the static nested class
        OuterClass1.StaticNestedClass nestedObject = new OuterClass1.StaticNestedClass();
        nestedObject.nestedClassInstanceMethod();
    }
}

/*
StaticNestedClass -> classMethod()
OuterClass1       -> classMethod()
StaticNestedClass -> instanceMethod()
 */