
package fundamentals;


import java.io.FileNotFoundException;
import java.text.ParseException;


public class ExceptionsExample1
{

    // throws checked exceptions
    private void sayHello() throws FileNotFoundException, ParseException
    {
    }

    // throws runtime exception
    private void sayHi() throws IllegalArgumentException
    {
    }

    // throws error
    private void sayGoodbye() throws AssertionError
    {
    }

    public static void main( String[] args )
    {
        ExceptionsExample1 obj = new ExceptionsExample1();

        try
        {
            obj.sayHello();
        }
        catch ( FileNotFoundException ex )
        {
            ex.printStackTrace();
        }
        catch ( ParseException ex )
        {
            ex.printStackTrace();
        }

        obj.sayHi();

        obj.sayGoodbye();
    }
}
