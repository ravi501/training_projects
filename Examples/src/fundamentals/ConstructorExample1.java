
package fundamentals;

abstract class CEParent
{
    public CEParent()
    {
        System.out.println( "Parent -> No-arg Constructor" );
    }

    public CEParent( String str )
    {
        System.out.println( "Parent -> 1-arg Constructor -> " + str );
    }
}


class CEChild extends CEParent
{
    public CEChild()
    {
        this( "XOXO" );
        System.out.println( "Child  -> No-arg Constructor" );
    }

    public CEChild( String str )
    {
        System.out.println( "Child  -> 1-arg Constructor -> " + str );
    }
}


public class ConstructorExample1
{

    public static void main( String[] args )
    {
        System.out.println( "Calling new Child()" );
        new CEChild();
        System.out.println();
        System.out.println( "Calling new Child(\"Hello, World!\")" );
        new CEChild( "Hello, World!" );
    }
}

/*
 * Calling new Child()
 * Parent -> No-arg Constructor
 * Child  -> 1-arg Constructor -> XOXO
 * Child  -> No-arg Constructor
 *
 * Calling new Child("Hello, World!")
 * Parent -> No-arg Constructor
 * Child  -> 1-arg Constructor -> Hello, World!
 */
