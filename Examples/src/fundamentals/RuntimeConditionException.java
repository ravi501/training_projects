/*
 * COPYRIGHT:
 *
 *   The computer systems, procedures, data bases and programs created and
 *   maintained by DST Systems, Inc., are proprietary in nature and as such
 *   are confidential.  Any unauthorized use or disclosure of such information
 *   may result in civil liabilities.
 *
 *   Copyright 2002 by DST Systems, Inc.
 *   All Rights Reserved.
 *******************************************************************************
 */

package com.dstsystems.infra.framework.condition;

import java.util.*;

import com.dstsystems.infra.framework.util.*;

/**
 * This class is the base for unchecked exceptions within the Infrastructure
 * Framework.  The RuntimeConditionException class extends the
 * java.lang.RuntimeException class to provide the following new behavior:
 *
 * 1. Externalized message text.
 * 2. Support for internationalization.
 * 3. Customizable message text via keyword substitutions.
 * 4. Message severity (warning, error, etc.).  @see Severity
 * 5. Support for exception chaining.
 *
 * @author    DST Systems, Inc.
 */
public class RuntimeConditionException extends RuntimeException
{
   /**
    * This variable holds the severity of the exception.
    */
   private int severity;

   /**
    * This variable holds the resource bundle name that was used at time of
    * object creation.  This is kept, in case the message needs to be rendered
    * in a different locale at a later time.
    */
   private String bundleId;

   /**
    * This variable holds message key that was used at time of object creation.
    * This is kept, in case the message needs to be rendered in a different
    * locale at a later time.
    */
   private String msgKey;

   /**
    * This variable holds the message parameters that were used at time of
    * object creation.  This is kept, in case the message needs to be rendered
    * in a different locale at a later time.  The message parameters are
    * key-value pairs such as:
    *
    *     new String [][]
    *     {
    *        { "key1", value1 },
    *        { "key2", value2 },
    *        .
    *        .
    *        .
    *
    *        { "keyN", valueN },
    *     }
    */
   private String[][] msgParameters;

   /**
    * This variable holds the cause of this exception.
    *
    * When the lowest supported version of java is JDK 1.4, this variable should
    * be deleted.  The reason is that a "cause" attribute was added to the
    * Throwable class in JDK 1.4.
    */
   private Throwable cause;

   /**
    * Creates a new instance of the class with given severity.
    *
    * @param  severity       severity level
    * @param  cause          a cause for the condition, can be null
    * @param  msgKey         key for actual message lookup in the given resource
    *                        bundle.  If the resource bundle name is null, the
    *                        <code>msgKey</code> will be used as the unformatted
    *                        message.
    * @param  msgParameters  two dimensional array [][2] of key-value pairs.
    *                        If null, then no substitution will be done in the
    *                        message.
    * @param  bundleName     resource bundle base name.  If null, the
    *                        <code>msgKey</code> will be used as the unformatted
    *                        message.
    * @param  locale         desired locale.  If null, the default locale will
    *                        be used.
    */
   public RuntimeConditionException( int severity, Throwable cause,
                                     String msgKey, String[][] msgParameters,
                                     String bundleName, Locale locale )
   {
      super( ResourceUtilities.makeMessage( msgKey, msgParameters, bundleName, locale ) );
      this.severity = severity;
      this.msgKey = msgKey;
      this.msgParameters = clone2( msgParameters );
      this.bundleId = bundleName;
      initCause( cause );
   }

   /**
    * Creates a new instance of the class with a default severity of ERROR.
    *
    * @param  cause          a cause for the condition, can be null
    * @param  msgKey         key for actual message lookup in the given resource bundle
    * @param  msgParameters  two dimensional array [][2] of key-value pairs.
    * @param  bundleName     resource bundle base name
    * @param  locale         desired locale.  If null, the default locale will
    *                        be used.
    */
   public RuntimeConditionException( Throwable cause,
                                     String msgKey, String[][] msgParameters,
                                     String bundleName, Locale locale )
   {
      this( Severity.ERROR, cause, msgKey, msgParameters, bundleName, locale );
   }

   /**
    * Returns the message key that was provided at object creation.
    *
    * @return The message key.
    */
   public String getMessageKey()
   {
      return msgKey;
   }

   /**
    * Returns null or String[][2], where keys are followed by values.
    *
    * @return message parameters
    */
   public String[][] getMsgParameters()
   {
      return clone2( msgParameters );
   }

   /**
    * Returns resource bundle name that was provided at object creation.
    *
    * @return name of resource bundle
    */
   public String getResourceBundleID()
   {
      return bundleId;
   }

   /**
    * This method can be used to render a message using a specific resource
    * bundle and default locale.
    *
    * @param resourceBundle
    *               resource bundle to use.
    *
    * @return message rendered using passed resource bundle and
    *         default locale.
    */
   public String getLocalizedMessage( ResourceBundle resourceBundle )
   {
      return ResourceUtilities.makeMessage( msgKey, msgParameters, resourceBundle );
   }

   /**
    * This method can be used to render a message using a specific bundle name
    * and locale.
    *
    * @param bundleName name of resource bundle to use.  If null, the resource
    *                   bundle that was passed at object creation will be
    *                   used.
    * @param locale     to use.  If null, the default locale will be used.
    *
    * @return message for given bundle name and locale.
    */
   public String getLocalizedMessage( String bundleName, Locale locale )
   {
      if ( bundleName == null )
      {
         bundleName = bundleId;
      }

      return ResourceUtilities.makeMessage( msgKey, msgParameters,
          bundleName, locale );
   }

   /**
    * This method implements 1.4 functionality for JDK 1.3.  Will be removed
    * from here when migration to 1.4 will is complete.  The method will be
    * available through its implementation in the java.lang.Throwable class.
    *
    * @return The cause of this exception.  The cause may be null.
    */
   public Throwable getCause()
   {
      return cause;
   }

   /**
    * This method implements 1.4 functionality for JDK 1.3.  Will be removed
    * from here when migration to 1.4 is complete.  The method will be available
    * through its implementation in the java.lang.Throwable class.
    *
    * @param  cause  place to put some previous exception to support chaining
    * @return        the same object, that was supplied as a parameter
    */
   public Throwable initCause( Throwable cause )
   {
      if ( this == cause )
      {
         throw new IllegalArgumentException( "Object cannot be a cause for itself" );
      }

      if ( this.cause != null )
      {
         throw new IllegalStateException( "Cause exists already" );
      }

      this.cause = cause;

      return cause;
   }

   /**
    * Sets the severity to a new value.
    *
    * @param severity The new severity value.
    *
    * @see Severity
    */
   public void setSeverity( int severity )
   {
      this.severity = severity;
   }

   /**
    * Gets the current severity.
    *
    * @return    severity
    */
   public int getSeverity()
   {
      return severity;
   }

   private static String [] [] clone2( String [] [] array )
   {
      String [] [] clone = null;

      if ( array != null )
      {
         clone = ( String [] [] ) array.clone();

         for ( int n = 0; n < array.length; ++n )
         {
            if ( array[ n ] != null )
            {
               if ( array[ n ].length != 2 )
               {
                  throw new IllegalArgumentException( "Parameter " + n +
                                                      " is not a key/value pair." );
               }

               clone[ n ] = ( String [] ) array[ n ].clone();
            }
         }
      }

      return clone;
   }
}

/*
 *
 * Name:
 *    %PM%
 *    %PID%
 *
 * Description:
 *    %PD%
 *
 * Design Parts:
 *    %PIRP%
 *
 * Last Changed:
 *    %PO%  -  %PRT%
 *
 * Changes:
 *    %PLA%
 *
 */
