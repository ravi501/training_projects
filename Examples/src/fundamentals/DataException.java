/*
 * COPYRIGHT:
 *
 *   The computer systems, procedures, data bases and programs created and
 *   maintained by DST Systems, Inc., are proprietary in nature and as such
 *   are confidential.  Any unauthorized use or disclosure of such information
 *   may result in civil liabilities.
 *
 *   Copyright 2002 by DST Systems, Inc.
 *   All Rights Reserved.
 *******************************************************************************
 */

package com.dstsystems.infra.framework.data;

import com.dstsystems.infra.framework.condition.*;
import java.util.Locale;

/**
 *  Root exception for the Data Layer.  Thrown when an exception occurs within
 *  a Data Layer component.
 *  <P>
 *  The messages are retrieved from the <code>DataMessages.properties</code>
 *  file that resides in the <code>com.dstsystems.infra.framework.data</code>
 *  package.
*/
public class DataException extends RuntimeConditionException
{
  // constants
  public static final String  BUNDLE_NAME = "com.dstsystems.infra.framework.data.DataMessages";

  /**
   *  Constructs exception and defaults:
   *  <UL>
   *  <LI>Locale to the locale on the client's machine.</LI>
   *  <LI>The bundle name to the <code>BUNDLE_NAME</code> constant.</LI>
   *  <LI>Severity to <code>Severity.ERROR</code></LI>
   *  </UL>
  */
  public DataException(Throwable cause, String messageKey,
      String [][] parameters)
  {
    super(Severity.ERROR, cause, messageKey, parameters, BUNDLE_NAME, null);
  }
}

/*
 *
 * Name:
 *    %PM%
 *    %PID%
 *
 * Description:
 *    %PD%
 *
 * Design Parts:
 *    %PIRP%
 *
 * Last Changed:
 *    %PO%  -  %PRT%
 *
 * Changes:
 *    %PLA%
 *
 */
