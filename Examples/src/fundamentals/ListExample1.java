
package fundamentals;


import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class ListExample1
{

    public static void main( String[] args )
    {
        List< Object > list = new ArrayList< Object >();
        list.add( 100 );
        list.add( new Integer( 200 ) );
        list.add( "hello" );
        list.add( new Date() );

        System.out.println( "Using 'for' to iterate thru list:" );
        for ( int i = 0; i < list.size(); i++ )
        {
            System.out.print( list.get( i ) + "   " );
        }

        System.out.println( "\nUsing enhanced 'for' to iterate thru list:" );
        for ( Object obj : list )
        {
            System.out.print( obj + "   " );
        }

        System.out.println( "\nUsing 'Iterator' to iterate thru list:" );
        Iterator< Object > listItr = list.iterator();
        while ( listItr.hasNext() )
        {
            Object obj = listItr.next();
            System.out.print( obj + "   " );
        }
    }
}

/*
Using 'for' to iterate thru list:
100   200   hello   Mon Nov 05 19:06:46 CST 2007   
Using enhanced 'for' to iterate thru list:
100   200   hello   Mon Nov 05 19:06:46 CST 2007   
Using 'Iterator' to iterate thru list:
100   200   hello   Mon Nov 05 19:06:46 CST 2007   
*/