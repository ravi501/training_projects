
package fundamentals;


import java.io.FileNotFoundException;
import java.io.IOException;


class IEParent
{

    // Constructor
    public IEParent()
    {
    }

    // Instance method
    protected void instanceMethod()
    {
        System.out.println( "IEParent -> instanceMethod()" );
    }

    // Static method
    protected static void classMethod()
    {
        System.out.println( "IEParent -> classMethod()" );
    }

    // ************************************************************************
    // Overloaded Method (or) Constructor:
    // * MUST have different argument lists
    // * May have different return types, if argument lists are also different
    // * May have different access modifiers
    // * May throw different exceptions
    // ************************************************************************

    // Overloaded constructor
    protected IEParent( String str ) throws Exception
    {
    }

    // Overloaded instance method
    IEParent instanceMethod( String name ) throws IOException
    {
        return null;
    }

    // Overloaded static method
    public static String classMethod( String str )
    {
        return str;
    }
}


class IEChild extends IEParent
{

    // Constructor
    public IEChild()
    {
    }

    // ************************************************************************
    // Overridden Method:
    // * MUST have the same argument list
    // * MUST have the same return type, or the return type can be a subclass
    // * MUST NOT have a more restrictive access modifier
    // * MUST NOT throw new or broader checked exceptions
    // ************************************************************************

    // Overridden instance method
    public void instanceMethod()
    {
        System.out.println( "IEChild  -> instanceMethod()" );
    }

    // Overridden instance method (Covariant return type)
    public IEChild instanceMethod( String name ) throws FileNotFoundException
    {
        return null;
    }

    // This static method could be *HIDDEN* by the superclass static method
    // depending on whether it is invoked from the superclass or subclass
    public static void classMethod()
    {
        System.out.println( "IEChild  -> classMethod()" );
    }
}


public class InheritanceExample1
{

    // NOTE:
    // Reference type determines Overloaded method used at Compile time.
    // Object type determines Overridden method used at Runtime.

    private static void doSomething( IEParent p )
    {
        System.out.println( "do something with Parent" );
    }

    private static void doSomething( IEChild c )
    {
        System.out.println( "do something with Child" );
    }

    public static void main( String[] args )
    {
        IEParent p1 = new IEParent();
        p1.instanceMethod();
        p1.classMethod();
        doSomething( p1 );

        System.out.println( "" );
        IEChild c1 = new IEChild();
        c1.instanceMethod();
        c1.classMethod();
        doSomething( c1 );

        System.out.println( "" );
        IEParent pc = new IEChild();
        pc.instanceMethod();
        pc.classMethod();
        doSomething( pc );
    }
}

/*
IEParent -> instanceMethod()
IEParent -> classMethod()
do something with Parent

IEChild  -> instanceMethod()
IEChild  -> classMethod()
do something with Child

IEChild  -> instanceMethod()
IEParent -> classMethod()
do something with Parent
*/