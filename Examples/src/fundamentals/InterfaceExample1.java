
package fundamentals;

interface MyThreadLikeInterface extends java.io.Serializable, Runnable
{

    public static final int VALUE1 = 100;
    int                     VALUE2 = 200;

    // throws no exceptions
    public abstract void sayHello();

    // throws checked exception
    void sayHi() throws java.io.IOException;

    // throws runtime & checked exceptions
    void sayGoodbye() throws NullPointerException, java.io.IOException;

    // Static methods are NOT permitted
    // static void SomeStaticMethod();
}


public class InterfaceExample1 extends Object implements MyThreadLikeInterface
{

    private static final long serialVersionUID = 1L;

    // Can declare "runtime" exceptions regardless of the interface declaration
    public void sayHello() throws NullPointerException
    {
    }

    // Can declare "checked" exceptions that are "narrower" than the exceptions
    // declared in the interface method
    public void sayHi() throws java.io.FileNotFoundException
    {
    }

    // Need not declare the exceptions of the interface
    public void sayGoodbye()
    {
    }

    // Runnable interface declares the run() method
    public void run()
    {
    }

    public static void main( String[] args )
    {
        System.out.format( "VALUE1 = %d%n", MyThreadLikeInterface.VALUE1 );
        System.out.format( "VALUE2 = %d%n", MyThreadLikeInterface.VALUE2 );
    }
}

/*
VALUE1 = 100
VALUE2 = 200
 */