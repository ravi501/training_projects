package exercise;

import java.util.Vector;
import java.util.Enumeration;
import java.io.File;
import java.io.FilenameFilter;

/**
 *
 * COPYRIGHT:
 *
 *   The computer systems, procedures, data bases and
 *   programs created and maintained by DST Systems, Inc.,
 *   are proprietary in nature and as such are confidential.
 *   Any unauthorized use or disclosure of such information
 *   may result in civil liabilities.
 *
 *   Copyright 1999 by DST Systems, Inc.
 *
 *******************************************************************************
 * <br><br>
 * A class which extends the FilenameFilter interface to handle getting lists
 * of files meeting the description passed in.
 *
 * @author  DST Systems, Inc.
 * @version %I%, %G%
 */

public class CppFilenameFilter extends Object implements FilenameFilter
{
    private static String TYPE_UNKNOWN = "Type Unknown";
    private static String HIDDEN_FILE = "Hidden File";
    private Vector _filter;

    /**
     * Creates a file filter. If no filters are added, then all
     * files are accepted.
     */
    public CppFilenameFilter()
    {
       _filter = new Vector();
    }

    /**
     * Creates a filename filter that accepts files with the given mask.
     * Example: new CppFilenameFilter("z*.jpg");
     * @param mask the mask to use
     * @see #addFilter
     */
    public CppFilenameFilter( String mask )
    {
       _filter = new Vector();
       addFilter( mask );
    }

    /**
     * Default finalize
     * @exception Throwable
     */
    protected void finalize() throws Throwable
    {
       _filter.removeAllElements();
       _filter = null;
       super.finalize();
    }
   
    /**
     * Return true if this file should be used,
     * false if it shouldn't.
     * Files that begin with "." are ignored.
     * @param f the File object
     * @param name the item asking to be accepted
     * @return true if the item should be used
     * @see #getExtension
     * @see FilenameFilter#accept
     */
    public boolean accept( File f, String name )
    {
       // no filter means accept all files
       if ( 0 == _filter.size() )
       {
          return true;
       }

       // if there is a file object to go against, operate
       if ( null != f )
       {
          // look for a match in the Vector of filters
          Enumeration e = _filter.elements();
          while ( e.hasMoreElements() )
          {
             String filter = (String)e.nextElement();
             // compare extensions
             String fileExtension = getExtension( name );
             String filterExtension = getExtension( filter );
             if ( compareStringToMask( fileExtension, filterExtension ) )
             {
                // ok, matched, so compare name
                String filename = getName( name );
                String filtername = getName( filter );
                if ( compareStringToMask( filename, filtername ) )
                {
                   return true;
                }
             }
          }
       }

       return false;
    }

    /**
     * Return the name portion of the file's name.
     * @param name the name to rip the extension from
     * @return the name part
     * @see #getName
     * @see CppFilenameFilter#accept
     */
    public static String getName( String name )
    {
       if ( null != name && 0 != name.length() )
       {
          int index = name.lastIndexOf( System.getProperty( "file.separator" ));
          if ( -1 != index )
          {
             name = name.substring( index + 1 );
          }

          int i = name.lastIndexOf( '.' );

          if ( -1 == i )
          {
             return name;
          }
          else
          {
             if ( i < name.length() - 1 )
             {
                return name.substring( 0, i ).toLowerCase();
             }
          }
       }
       return null;
    }

    /**
     * Return the extension portion of the file's name.
     * @param name the name to rip the extension from
     * @return the extension
     * @see #getExtension
     * @see CppFilenameFilter#accept
     */
    public static String getExtension( String name )
    {
       if ( null != name && 0 != name.length() )
       {
          int i = name.lastIndexOf( '.' );

          // if no dot, then i = -1 which means there is no extension
          if ( i >= 0 && i < name.length() - 1 )
          {
             return name.substring( i + 1 ).toLowerCase();
          }
       }
       return null;
    }

    /**
     * Return true if the string matches the mask. Mask is defined to be
     * any legal filename string with 0 or 1 * chars in it.
     * @param s the string to match
     * @param mask the mask to compare to
     * @return true or false
     * @see CppFilenameFilter#accept
     */
    public boolean compareStringToMask( String s, String mask )
    {
       // do the quick compares first
       if ( ( null == s || ( null != s && 0 == s.length() ) ) &&
            ( null != mask && 0 != mask.length() ) )
       {
          return false;
       }

       if ( ( null != s && 0 != s.length() ) &&
            ( null == mask || ( null != mask && 0 == mask.length() ) ) )
       {
          return true;
       }

       if ( null != s    && 0 != s.length() &&
            null != mask && 0 != mask.length() )
       {
          int curMaskIndex = 0;

          curMaskIndex = mask.indexOf( '*' );

          // if no star, just compare the strings directly
          if ( -1 == curMaskIndex )
          {
             if ( s.equals( mask ) )
             {
                return true;
             }
          }
          else if ( 0 != curMaskIndex )
          {
             // otherwise, compare up to the *
             String sSubMask = mask.substring( 0, curMaskIndex );
             if ( false == s.startsWith( sSubMask ) )
             {
                // the match failed, so quit
                return false;
             }
          }

          // this means there are characters after the *
          if ( curMaskIndex < mask.length() - 1 )
          {
             String sSubMask = mask.substring( curMaskIndex + 1 );
             if ( false == s.endsWith( sSubMask ) )
             {
                // the match failed, so quit
                return false;
             }
          }
       }
       return true;
    }

    /**
     * Adds a filter to filter against.
     *
     * For example: the following code will create a filter that filters
     * out all files except those that end in ".jpg" ".tif", ".gif",
     * "a*.pcx", and "z*.p*":
     *
     *   CppFilenameFilter filter = new CppFilenameFilter();
     *   filter.addFilter( "jpg" );
     *   filter.addFilter( "tif" );
     *   filter.addFilter( "*.gif" );
     *   filter.addFilter( "a*.pcx" );
     *   filter.addFilter( "z*.p*" );
     *
     * @param str the filter to add
     */
    public void addFilter( String str )
    {
       int index = str.lastIndexOf( System.getProperty( "file.separator" ) );
       if ( -1 != index )
       {
          str = str.substring( index + 1 );
       }

       _filter.addElement( str.substring( 0 ).toLowerCase() );
    }
}


/*
 *
 * Name:
 *    %PM%
 *    %PID%
 *
 * Description:
 *    %PD%
 *
 * Design Parts:
 *    %PIRP%
 *
 * Last Changed:
 *    %PO%  -  %PRT%
 *
 * Changes:
 *    %PLA%
 *
*/

