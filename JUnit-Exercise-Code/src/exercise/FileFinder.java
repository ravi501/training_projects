package exercise;

import java.io.File;

import java.util.StringTokenizer;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;

/**
 *
 * COPYRIGHT:
 *
 *   The computer systems, procedures, data bases and
 *   programs created and maintained by DST Systems, Inc.,
 *   are proprietary in nature and as such are confidential.
 *   Any unauthorized use or disclosure of such information
 *   may result in civil liabilities.
 *
 *   Copyright 1999 by DST Systems, Inc.
 *
 *******************************************************************************
 * <br><br>
 * A class which attempt to find a file anywhere under a given a set of base
 * directories (follows the normal PATH conventions).
 *
 * @author  DST Systems, Inc.
 * @version %I%, %G%
 */

public class FileFinder extends Object
{
    /**
     * Creates a file finder.
     */
    public FileFinder()
    {
    }

    /**
     * Given a list of base paths and a filename, this method recursively
     * searches the base paths for the given file.
     * @param pathList a set of fully-qualified base paths, separated by the
     *                 platform's pathSeparator character
     * @param filename a filename with an optional extension
     * @return fully-qualified filename or null if not found
     */
    public static String findFile( String pathList, String filename )
    {
       return findFile( pathList, filename, null );
    }

    /**
     * Given a list of base paths and a filename, this method recursively
     * searches the base paths for the given file.
     * @param  pathList a set of fully-qualified base paths, separated by the
     *                   platform's pathSeparator character
     * @param  filename  a filename with an optional extension
     * @param  extraPath a path to find the file in which is applied to each
     *                   path in turn
     * @return fully-qualified filename or null if not found
     */
    public static String findFile( String pathList, String filename,
                                   String extraPath )
    {
       // now cycle through the paths in sourcePath to try to find the file
       StringTokenizer st = new StringTokenizer( pathList, File.pathSeparator );
       while ( st.hasMoreTokens() )
       {
          // grab a path from the path list
          String path = st.nextToken();

          // try to create a direct link to the file
          File f = null;

          if ( null == extraPath )
          {
             f = new File( path, filename );
          }
          else
          {
             f = new File( path + File.separatorChar + extraPath, filename );
          }

          if ( f.exists() )
          {
             return f.getAbsolutePath();
          }

          // create a File object which knows about just the path
          f = new File( path );

          if ( null != f )
          {
             // ask the File object for a list of files/directories
             String[] sl = f.list();

             if ( null != sl )
             {
                // for each entry in the list, create a File object to see if
                // it is a directory. If so, recurse!
                for ( int index = 0; index < sl.length; index++ )
                {
                   File f2 = new File( path, sl[ index ] );
                   if ( f2.isDirectory() )
                   {
                      String subPath = path + File.separatorChar + sl[ index ];
                      String foundFile = findFile( subPath, filename );
                      if ( null != foundFile )
                      {
                         return foundFile;
                      }
                   }
                }
             }
          }
       }
       return null;
    }

    /**
     * Given a base path and a filter, this method recursively
     * searches the base path for all files meeting the filter.
     * @param pathList a fully-qualified base path
     * @param filter a String such as '*.h*'
     * @param foundList a created Vector where the list will be added to.
     *                  this is passed-in so that findAllFiles can be called
     *                  multiple times to build a single Vector for multiple
     *                  filters.
     */
    public static void findAllFiles( String basePath, String filter,
                                     Vector foundList )
    {
       findAllFiles( basePath, filter, foundList, true );
    }

    /**
     * Given a base path and a filter, this method recursively
     * searches the base path for all files meeting the filter.
     * @param pathList a fully-qualified base path
     * @param filter a String such as '*.h*'
     * @param foundList a created Vector where the list will be added to.
     *                  this is passed-in so that findAllFiles can be called
     *                  multiple times to build a single Vector for multiple
     *                  filters.
     * @param lower     true to lowercase the results
     */
    public static void findAllFiles( String basePath, String filter,
                                     Vector foundList, boolean lower )
    {
       // now cycle through the paths in sourcePath to try to find the file
       StringTokenizer st = new StringTokenizer( basePath, File.pathSeparator );
       while ( st.hasMoreTokens() )
       {
          // grab a path from the path list
          String path = st.nextToken();
          if ( lower )
          {
             path = path.toLowerCase();
          }

          // create a File object which knows about just the path
          File f = new File( path );

          if ( null != f )
          {
             // ask the File object for a list of files/directories
             CppFilenameFilter cff = new CppFilenameFilter( filter );
             // sl is the files that match the filter
             String[] sl = f.list( cff );

             if ( null != sl )
             {
                // add the Strings to the return Vector
                for ( int index = 0; index < sl.length; index++ )
                {
                   if ( lower )
                   {
                      foundList.addElement( path + File.separatorChar +
                                            sl[ index ].toLowerCase() );
                   }
                   else
                   {
                      foundList.addElement( path + File.separatorChar +
                                            sl[ index ] );
                   }
                }

                // slDirs is everything in the directory so we can check for
                // more directories
                String[] slDirs = f.list();

                // if either one has entries, continue
                if ( 0 != slDirs.length )
                {
                   // for each entry in the list, create a File object to see if
                   // it is a directory. If so, recurse!
                   for ( int index = 0; index < slDirs.length; index++ )
                   {
                      File f2 = new File( path, slDirs[ index ] );
                      if ( f2.isDirectory() )
                      {
                         String subPath = path + File.separatorChar +
                                          slDirs[ index ];
                         findAllFiles( subPath, filter, foundList, lower );
                      }
                   }
                }
             }
          }
       }
    }

    /**
     * Given a list of base paths and a filename, this method recursively
     * searches the base paths for the given file.
     * @param  fileTree a tree of files in a Vector, with the fully-qualified
     *                  names of files and Vectors for each directory
     * @param  filename  a filename with an optional extension
     * @return fully-qualified filename or null if not found
     */
    public static String findFile( Vector fileTree, String filename )
    {
       return findFile( fileTree, filename, null, false );
    }

    /**
     * Given a list of base paths and a filename, this method recursively
     * searches the base paths for the given file.
     * @param  fileTree a tree of files in a Vector, with the fully-qualified
     *                  names of files and Vectors for each directory
     * @param  filename  a filename with an optional extension
     * @param  extraPath a path to prepend to the filename being searched for
     * @return fully-qualified filename or null if not found
     */
    public static String findFile( Vector fileTree, String filename,
                                   String extraPath, boolean lower )
    {
       Enumeration iter = fileTree.elements();
       while ( iter.hasMoreElements() )
       {
          String s = (String)iter.nextElement();
          String compareTo = s;
          if ( lower )
          {
             filename = filename.toLowerCase();
          }

          int index = s.lastIndexOf( File.separatorChar );
          if ( -1 != index )
          {
             if ( null != extraPath )
             {
                String temp = s.substring( 0, index );
                index = temp.lastIndexOf( File.separatorChar );
             }

             compareTo = s.substring( index + 1 );
          }

          if ( null == extraPath )
          {
             if ( compareTo.equals( filename ) )
             {
                return s;
             }
          }
          else
          {
             if ( lower )
             {
                extraPath = extraPath.toLowerCase();
             }

             if ( compareTo.equals( extraPath + File.separatorChar +
                                    filename ) )
             {
                return s;
             }
          }
       }

       return null;
    }

    /**
     * Finds all the files for a given mask
     * @param argv the list of arguments to this method passed in from the
     *        command line
     */
    public static void main( String argv[] )
    {
       String fullFilename = "";
       String headerFilename = "";
       String sourcePath = "";
       String className = "";
       String packageName = null;
       String filePath = null;
       String usage = "Usage: FileFinder -f:mask [-p:path]";

       if ( argv.length > 0 )
       {
          for ( int count = 0; count < argv.length; count++ )
          {
             if ( 0 == argv[count].length() )
             {
                System.out.println( usage );
                return;
             }

             int index = -1;

             switch ( argv[count].charAt( 1 ) )
             {
             case 'f':
                fullFilename = argv[count].substring( 3 );
                int i = fullFilename.lastIndexOf( File.separator );
                if ( i > 0 && i < fullFilename.length() - 1 )
                {
                   if ( fullFilename.charAt( i - 1 ) == ':' )
                   {
                      filePath = fullFilename.substring( 0, i + 1 );
                   }
                   else
                   {
                      filePath = fullFilename.substring( 0, i );
                   }

                   headerFilename = fullFilename.substring( i + 1 );
                }
                else
                {
                   filePath = Env.dir;
                   headerFilename = fullFilename.substring( 0 );
                }
                break;

             case 'p':
                filePath = argv[count].substring( 3 );
                break;
             }
          }

          if ( 0 == fullFilename.length() )
          {
             System.out.println( usage );
          }
          else
          {
             Vector fileList = new Vector();

             if ( -1 != fullFilename.indexOf( '*' ) )
             {
                if ( null != filePath )
                {
                   try
                   {
                      FileFinder.findAllFiles( filePath, fullFilename,
                                               fileList, false );

                      if ( 0 == fileList.size() )
                      {
                         fileList.addElement( fullFilename );
                      }
                   }
                   catch ( Exception e )
                   {
                      System.out.println( "can't get file list for '" +
                                          filePath + "'" );
                   }
                }
             }
             else
             {
                fileList.addElement( fullFilename );
             }

             Enumeration iter = fileList.elements();
             while ( iter.hasMoreElements() )
             {
                System.out.println( (String)iter.nextElement() );
             }

             System.out.println( "" + fileList.size() + " files found" );

             System.out.println( "" + FileFinder.findFile( fileList, "ctype.h",
                                                           null, false ) );
             System.out.println( "" + FileFinder.findFile( fileList, "ctype.h",
                                                           "rw", false ) );

             fileList.removeAllElements();
          }
       }
       else
       {
          System.out.println( usage );
       }
    }
}


/*
 *
 * Name:
 *    %PM%
 *    %PID%
 *
 * Description:
 *    %PD%
 *
 * Design Parts:
 *    %PIRP%
 *
 * Last Changed:
 *    %PO%  -  %PRT%
 *
 * Changes:
 *    %PLA%
 *
*/

