package exercise;

import java.lang.*;


public class Env
{
// Global Data
   public static final String       newLine =
                                         System.getProperty( "line.separator" );
   public static final String       opSys = System.getProperty( "os.name" );
   public static final String       user = System.getProperty( "user.name" );
   public static final String       dir = System.getProperty( "user.dir" );


   public static void main( String args[] )
   {
      System.out.println( "newLine: " + Env.newLine );
      System.out.println( "opSys: " + Env.opSys );
      System.out.println( "user: " + Env.user );
      System.out.println( "dir: " + Env.dir );
   }
}


/*
 *
 * Name:
 *    %PM%
 *    %PID%
 *
 * Description:
 *    %PD%
 *
 * Design Parts:
 *    %PIRP%
 *
 * Last Changed:
 *    %PO%  -  %PRT%
 *
 * Changes:
 *    %PLA%
 *
*/

