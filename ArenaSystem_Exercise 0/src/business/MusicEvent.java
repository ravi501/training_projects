
package business;

public class MusicEvent extends Event
{
    public void populateFlags()
    {
        super.populateFlags();
        setPetsAllowed( 'N' );
        setChewingAllowed( 'N' );
        setSmokingAllowed( 'N' );
        setVideosAllowed( 'N' );
        setAudioAllowed( 'N' );
        setPhotosAllowed( 'Y' );
    }
}
