
package business;

public class BasketBallEvent extends SportsEvent
{
    public void populateFlags()
    {
        super.populateFlags();
        setPetsAllowed( 'N' );
        setChewingAllowed( 'N' );
        setSmokingAllowed( 'N' );
        setVideosAllowed( 'N' );
        setAudioAllowed( 'Y' );
        setPhotosAllowed( 'N' );
    }
}
