
package business;

public class SportsEvent extends Event
{
    public void populateFlags()
    {
        setWineAllowed( 'N' );
        setBeerAllowed( 'Y' );
    }
}
