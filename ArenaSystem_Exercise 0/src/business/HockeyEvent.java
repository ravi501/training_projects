
package business;

public class HockeyEvent extends Event
{
    public void populateFlags()
    {
        setPetsAllowed( 'N' );
        setChewingAllowed( 'Y' );
        setSmokingAllowed( 'N' );
        setVideosAllowed( 'Y' );
        setAudioAllowed( 'Y' );
        setPhotosAllowed( 'N' );
    }

}
