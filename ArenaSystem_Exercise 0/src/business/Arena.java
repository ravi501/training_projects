
package business;


import java.util.ArrayList;

import data.EventData;


public class Arena
{
    // TODO Exercise 3 - Look in this class to see if you can apply any of these
    // rules of thumb:
    // TODO Exercise 3a - Don�t branch too much (by using too many ifs or
    // switches); may need to subclass
    // TODO Exercise 3b - Don�t repeat code; develop another method that handles
    // repetitive code
    // TODO Exercise 3c - Move code up to superclass if multiple subclasses use
    // it

    // TODO Exercise 4 (concatenation) - Look in this class for concatenated
    // statements that are difficult to read/understand

    // TODO Exercise 5a - Look in this class for multiple returns in a method

    // TODO Exercise 5c - Look in this class for:
    // * Bad data names
    // * Bad method names
    // * Long methods that need to be split up
    // * Logic that just seems illogical

    int                i           = 0;
    int                i2          = 0;
    ArrayList< Event > EventsArray = new ArrayList< Event >();

    public void processEvents()
    {
        // Get an event record from the EventData class
        String giveMeAnotherEventForProcessing = retrieveAnEventRecordFromEventData();

        // Process all event records
        do
        {
            // Parse the event record, and then add to the ArrayList
            EventsArray.add( parse( giveMeAnotherEventForProcessing ) );

            // Retrieve another event record
            giveMeAnotherEventForProcessing = retrieveAnEventRecordFromEventData();

        }
        while ( giveMeAnotherEventForProcessing != null ); // As long as
                                                           // there are
                                                           // records

        // Get another event from the ArrayList, and then validate it
        for ( int i = 0; i < EventsArray.size(); i++ )
        {
            populateEventFlags( getEvent() );
        }
    }

    // What's wrong with the ValidateEvent method?
    // If changes are needed, implement changes accordingly. Be prepared to
    // answer why changes were, or were not, necessary

    // Consider these rules of thumb...
    // TODO Exercise 2 - Methods should have one and only one function
    //
    public boolean populateEventFlags( Event myEvent )
    {
        boolean result = false;

        // Validate event types
        if ( isValidEvent( myEvent ) )
        { // valid events

            // Responsibility is being passed back to the event
            // which is where the data is
            myEvent.populateFlags();

            result = true;
        }

        return result; // Only reaches this code if invalid type
    }

    private boolean isValidEvent( Event myEvent )
    {

        return myEvent != null;
    }

    // Retrieves the next event from the EventsArray collection
    private Event getEvent()
    {
        // Start at current index of my global i2 index
        int nextEvent = i2;

        // Build a new Event
        Event myEvent = Event
                .instantiateEvent( EventsArray.get( nextEvent ).eventType );

        // Go to next index
        i2++;

        return myEvent;
    }

    // This method parses an event record by calling the EventData class's
    // parseEventRecord
    private Event parse( String event )
    {
        return EventData.parseEventRecord( event );
    }

    // Retrieve a single event record, a String, from the EventData class
    private String retrieveAnEventRecordFromEventData()
    {
        // Get an event record; i++ increments the counter AFTER retrieving
        // the data from the array

        return EventData.eventDataArray[ i++ ];
    }

    // Print all my Events
    public void printEvents()
    {
        for ( int i = 0; i < EventsArray.size(); i++ )
        {
            System.out.println( EventsArray.get( i ) );
        }
    }
}
