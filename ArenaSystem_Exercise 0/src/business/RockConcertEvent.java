
package business;

public class RockConcertEvent extends MusicEvent
{
    public void populateFlags()
    {
        super.populateFlags();
        setWineAllowed( 'N' );
        setBeerAllowed( 'Y' );
    }
}
