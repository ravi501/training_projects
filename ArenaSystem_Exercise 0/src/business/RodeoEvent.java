
package business;

public class RodeoEvent extends SportsEvent
{
    public void populateFlags()
    {
        super.populateFlags();
        setPetsAllowed( 'Y' );
        setChewingAllowed( 'Y' );
        setSmokingAllowed( 'N' );
        setVideosAllowed( 'Y' );
        setAudioAllowed( 'Y' );
        setPhotosAllowed( 'Y' );
    }
}
