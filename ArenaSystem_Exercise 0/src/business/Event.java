
package business;

public class Event
{
    // TODO Exercise 1b - Look in this class for bad data names; consider
    // getters & setters, too
    // TODO Exercise 4 (concatenation) - Look in this class for concatenated
    // statements that are difficult to read/understand
    // TODO Exercise 5b - Look in this class for multiple returns in a method
    // TODO Exercise 5d - Look in this class for:
    // * Long methods that need to be split up
    // * Logic that just seems illogical

    // Name for event
    String eventName;

    // Contact info for event
    String contactName;
    String contactPhone;
    String contactAddress;
    String contactCity;
    String contactState;
    String contactZip;
    String contactEmail;

    // Type of event (basketball, hockey, rodeo, rock concert, symphony concert)
    String eventType;

    // Type of activities allowed
    char   beerAllowed;
    char   wineAllowed;
    char   smokingAllowed;
    char   chewingAllowed;
    char   videosAllowed;
    char   photosAllowed;
    char   audioAllowed;
    char   petsAllowed;

    // Start & end date & time
    String startDate;
    String endDate;

    // Capacities, min & max (min N/A for sports events--hockey, basketball, or
    // rodeo--or symphony concerts)
    int    rockMinimumCapacity;
    int    rockMaximumCapacity;

    // Parking prices (all tiers of parking are free for symphony concerts)
    double parkingPriceTier1;  // Tier 1 (common)
    double parkingPriceTier2;  // Tier 2 (VIP)
    double parkingPriceTier3;  // Tier 3 (physically challenged)
    double parkingPriceTier4;  // Tier 4 (covered parking)

    // Placement of stage/equipment (N/A for sports events: hockey, basketball,
    // & rodeo)
    String stagePlacement;     // stage

    String northAmpPlacement;  // north amps
    String southAmpPlacement;  // south amps
    String eastAmpPlacement;   // east amps
    String westAmpPlacement;   // west amps

    // Getters and setters
    public String getEventName()
    {
        return eventName;
    }

    public void setEventName( String eventName )
    {
        this.eventName = eventName;
    }

    public String getContactName()
    {
        return contactName;
    }

    public void setContactName( String cname )
    {
        this.contactName = cname;
    }

    public String getContactPhone()
    {
        return contactPhone;
    }

    public void setContactPhone( String contactPhone )
    {
        this.contactPhone = contactPhone;
    }

    public String getContactAddress()
    {
        return contactAddress;
    }

    public void setContactAddress( String contactAddress )
    {
        this.contactAddress = contactAddress;
    }

    public String getContactCity()
    {
        return contactCity;
    }

    public void setContactCity( String contactCity )
    {
        this.contactCity = contactCity;
    }

    public String getContactState()
    {
        return contactState;
    }

    public void setContactState( String contactState )
    {
        this.contactState = contactState;
    }

    public String getContactZip()
    {
        return contactZip;
    }

    public void setContactZip( String contactZip )
    {
        this.contactZip = contactZip;
    }

    public String getContactEmail()
    {
        return contactEmail;
    }

    public void setContactEmail( String contactEmail )
    {
        this.contactEmail = contactEmail;
    }

    public String getEventType()
    {
        return eventType;
    }

    public void setEventType( String eventType )
    {
        this.eventType = eventType;
    }

    public char getBeerAllowed()
    {
        return beerAllowed;
    }

    public void setBeerAllowed( char beerActivity )
    {
        this.beerAllowed = beerActivity;
    }

    public char getWineAllowed()
    {
        return wineAllowed;
    }

    public void setWineAllowed( char wineActivity )
    {
        this.wineAllowed = wineActivity;
    }

    public char getSmokingAllowed()
    {
        return smokingAllowed;
    }

    public void setSmokingAllowed( char smokingActivity )
    {
        this.smokingAllowed = smokingActivity;
    }

    public char getChewingAllowed()
    {
        return chewingAllowed;
    }

    public void setChewingAllowed( char chewingActivity )
    {
        this.chewingAllowed = chewingActivity;
    }

    public char getVideosAllowed()
    {
        return videosAllowed;
    }

    public void setVideosAllowed( char videosActivity )
    {
        this.videosAllowed = videosActivity;
    }

    public char getPhotosAllowed()
    {
        return photosAllowed;
    }

    public void setPhotosAllowed( char photosActivity )
    {
        this.photosAllowed = photosActivity;
    }

    public char getAudioAllowed()
    {
        return audioAllowed;
    }

    public void setAudioAllowed( char audioActivity )
    {
        this.audioAllowed = audioActivity;
    }

    public char getPetsAllowed()
    {
        return petsAllowed;
    }

    public void setPetsAllowed( char petsActivity )
    {
        this.petsAllowed = petsActivity;
    }

    public String getStartDate()
    {
        return startDate;
    }

    public void setStartDate( String startDate )
    {
        this.startDate = startDate;
    }

    public String getEndDate()
    {
        return endDate;
    }

    public void setEndDate( String endDate )
    {
        this.endDate = endDate;
    }

    public int getRockMinimumCapacity()
    {
        return rockMinimumCapacity;
    }

    public void setRockMinimumCapacity( int rockMinCapacity )
    {
        this.rockMinimumCapacity = rockMinCapacity;
    }

    public int getRockMaximumCapacity()
    {
        return rockMaximumCapacity;
    }

    public void setRockMaximumCapacity( int rockMaxCapacity )
    {
        this.rockMaximumCapacity = rockMaxCapacity;
    }

    public double getParkingPriceTier1()
    {
        return parkingPriceTier1;
    }

    public void setParkingPriceTier1( double parkingPrice1 )
    {
        this.parkingPriceTier1 = parkingPrice1;
    }

    public double getParkingPriceTier2()
    {
        return parkingPriceTier2;
    }

    public void setParkingPriceTier2( double parkingPrice2 )
    {
        this.parkingPriceTier2 = parkingPrice2;
    }

    public double getParkingPriceTier3()
    {
        return parkingPriceTier3;
    }

    public void setParkingPriceTier3( double parkingPrice3 )
    {
        this.parkingPriceTier3 = parkingPrice3;
    }

    public double getParkingPriceTier4()
    {
        return parkingPriceTier4;
    }

    public void setParkingPriceTier4( double parkingPrice4 )
    {
        this.parkingPriceTier4 = parkingPrice4;
    }

    public String getStagePlacement()
    {
        return stagePlacement;
    }

    public void setStagePlacement( String stagePlacement )
    {
        this.stagePlacement = stagePlacement;
    }

    public String getNorthAmpPlacement()
    {
        return northAmpPlacement;
    }

    public void setNorthAmpPlacement( String placeAmp1 )
    {
        this.northAmpPlacement = placeAmp1;
    }

    public String getSouthAmpPlacement()
    {
        return southAmpPlacement;
    }

    public void setSouthAmpPlacement( String placeAmp2 )
    {
        this.southAmpPlacement = placeAmp2;
    }

    public String getEastAmpPlacement()
    {
        return eastAmpPlacement;
    }

    public void setEastAmpPlacement( String placeAmp3 )
    {
        this.eastAmpPlacement = placeAmp3;
    }

    public String getWestAmpPlacement()
    {
        return westAmpPlacement;
    }

    public void setWestAmpPlacement( String placeAmp4 )
    {
        this.westAmpPlacement = placeAmp4;
    }

    // Build a meaningful string from an Event.
    // In theory, every class should have a toString() method for the purpose of
    // printing out the values within an instance of the class. Otherwise, the
    // toString() method of the Object class will be used, via inheritance.
    public String toString()
    {
        StringBuilder myString = new StringBuilder( "\n\n" );

        myString.append( eventName );
        myString.append( "\n" );
        myString.append( contactName );
        myString.append( "\n" );
        myString.append( contactPhone );
        myString.append( "\n" );
        myString.append( contactAddress );
        myString.append( "\n" );
        myString.append( contactCity );
        myString.append( contactState );
        myString.append( contactZip );
        myString.append( "\n" );
        myString.append( contactEmail );
        myString.append( "\n" );
        myString.append( eventType );
        myString.append( "\n" );
        myString.append( beerAllowed );
        myString.append( wineAllowed );
        myString.append( smokingAllowed );
        myString.append( chewingAllowed );
        myString.append( videosAllowed );
        myString.append( photosAllowed );
        myString.append( audioAllowed );
        myString.append( petsAllowed );
        myString.append( "\nStart: " );
        myString.append( startDate );
        myString.append( "\nEnd:   " );
        myString.append( endDate );
        myString.append( "\nMin:   " );
        myString.append( rockMinimumCapacity );
        myString.append( "\nMax:   " );
        myString.append( rockMaximumCapacity );
        myString.append( "\nParking Prices...\n" );
        myString.append( parkingPriceTier1 );
        myString.append( "\n" );
        myString.append( parkingPriceTier2 );
        myString.append( "\n" );
        myString.append( parkingPriceTier3 );
        myString.append( "\n" );
        myString.append( parkingPriceTier4 );
        myString.append( "\nStage/Amp Placement...\n" );
        myString.append( stagePlacement );
        myString.append( "\n" );
        myString.append( northAmpPlacement );
        myString.append( "\n" );
        myString.append( southAmpPlacement );
        myString.append( "\n" );
        myString.append( eastAmpPlacement );
        myString.append( "\n" );
        myString.append( westAmpPlacement );
        myString.append( "\n" );

        return myString.toString();
    }

    public void populateFlags()
    {

    }

    public static Event instantiateEvent( String type )
    {
        Event anEvent = null;

        if ( "BasketBall".equals( type ) )
        {
            anEvent = new BasketBallEvent();
        }
        else if ( "Hockey".equals( type ) )
        {
            anEvent = new HockeyEvent();
        }
        else if ( "Rodeo".equals( type ) )
        {
            anEvent = new RodeoEvent();
        }
        else if ( "Rock".equals( type ) )
        {
            anEvent = new RockConcertEvent();
        }
        else if ( "Symphony".equals( type ) )
        {
            anEvent = new SymphonyEvent();
        }
        else
        {
            System.out.println( "Invalid event" );
        }
        return anEvent;
    }

}
