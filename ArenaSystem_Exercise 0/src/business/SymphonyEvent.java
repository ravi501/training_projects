
package business;

public class SymphonyEvent extends MusicEvent
{
    public void populateFlags()
    {
        super.populateFlags();
        setWineAllowed( 'Y' );
        setBeerAllowed( 'N' );
    }
}
