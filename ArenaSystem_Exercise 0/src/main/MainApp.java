package main;
import business.Arena;


public class MainApp {

	// TODO Exercise 1a - Look in this class for bad names

	public static void main(String[] args) {
		// Instantiate an object of the arena class
		Arena arena = new Arena();

		// Process all Events for the arena
		arena.processEvents();
		
		// Print all Events
		arena.printEvents();
	}

}
