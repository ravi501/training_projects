package data;
import business.Event;

// The EventData class contains the data and the parsing code for the data.
//
// Please don't change code in the EventData class without discussing with the instructor.

public final class EventData {
	private static String myEventRecord; // Event record used for parsing
	private static Event myEvent; // Used for building the Event for callers

	public static String[] eventDataArray = {
			"U2        Derek White    8168439156816 Broadway  Kansas City    MO64105dbwhite@dstsystems.com     02/07/201419:0002/07/201422:00Rock          18155100001001001801501101501501101501902500500000004000",
			"PBR       Aaron Diffender8168439202816 Broadway  Kansas City    MO64105atdiff@dstsystems.com      08/11/201200:0008/16/201224:00Rodeo         1729800000                              1500300005002500",
			"KC Sympho1John Pfeiffer  8168439315816 Broadway  Kansas City    MO64105japfeiffer@dstsystems.com  06/13/201210:0006/16/201222:00Symphony      18355000001001001801501101501501101501900000300005002500",
			"KC Sympho2John Pfeiffer  8168439315816 Broadway  Kansas City    MO64105japfeiffer@dstsystems.com  07/11/201210:0007/14/201222:00Symphony      18355000001001001801501101501501101501900000300005002500",
			"KC Sympho3John Pfeiffer  8168439315816 Broadway  Kansas City    MO64105japfeiffer@dstsystems.com  08/15/201210:0008/18/201222:00Symphony      18355000001001001801501101501501101501900000300005002500" };

	public static Event parseEventRecord(String eventRecord) {
		myEventRecord = eventRecord;

		return buildEvent();
	}

	private static Event buildEvent() {
		myEvent = new Event();

		populateEvent();

		return myEvent;
	}

	// Please don't change code in the EventData class without discussing with
	// the instructor.
	private static void populateEvent() {
		myEvent.setEventName(getEventName());
		myEvent.setContactName(getContactName());
		myEvent.setContactPhone(getContactPhone());
		myEvent.setContactAddress(getContactAddress());
		myEvent.setContactCity(getContactCity());
		myEvent.setContactState(getContactState());
		myEvent.setContactZip(getContactZIP());
		myEvent.setContactEmail(getContactEmail());
		myEvent.setStartDate(getStartDateTime());
		myEvent.setEndDate(getEndDateTime());
		myEvent.setEventType(getEventType());
		myEvent.setRockMaximumCapacity(getMaxCapacity());
		myEvent.setRockMinimumCapacity(getMinCapacity());
		myEvent.setStagePlacement(getStagePlacement());
		myEvent.setNorthAmpPlacement(getAmpPlacementEast());
		myEvent.setSouthAmpPlacement(getAmpPlacementWest());
		myEvent.setEastAmpPlacement(getAmpPlacementNorth());
		myEvent.setWestAmpPlacement(getAmpPlacementSouth());
		myEvent.setParkingPriceTier1(getParkingPriceCommon());
		myEvent.setParkingPriceTier2(getParkingPriceVIP());
		myEvent.setParkingPriceTier3(getParkingPriceChallenged());
		myEvent.setParkingPriceTier4(getParkingPriceCovered());
	}

	// Please don't change code in the EventData class without discussing with
	// the instructor.
	private static Double getParkingPriceCommon() {
		return Double.parseDouble(myEventRecord.substring(182, 186)) / 100;
	}

	private static Double getParkingPriceVIP() {
		return Double.parseDouble(myEventRecord.substring(186, 190)) / 100;
	}

	private static Double getParkingPriceChallenged() {
		return Double.parseDouble(myEventRecord.substring(190, 194)) / 100;
	}

	private static Double getParkingPriceCovered() {
		return Double.parseDouble(myEventRecord.substring(194, 198)) / 100;
	}

	// Please don't change code in the EventData class without discussing with
	// the instructor.
	private static String getStagePlacement() {
		return myEventRecord.substring(152, 158);
	}

	private static String getAmpPlacementEast() {
		return myEventRecord.substring(158, 164);
	}

	private static String getAmpPlacementWest() {
		return myEventRecord.substring(164, 170);
	}

	private static String getAmpPlacementNorth() {
		return myEventRecord.substring(170, 176);
	}

	private static String getAmpPlacementSouth() {
		return myEventRecord.substring(176, 182);
	}

	// Please don't change code in the EventData class without discussing with
	// the instructor.
	private static int getMinCapacity() {
		String minCapString = myEventRecord.substring(147, 152);
		Integer minCapacity = null;

		if (!minCapString.equals(" ")) {
			minCapacity = Integer.parseInt(minCapString);
		}

		return minCapacity;
	}

	private static int getMaxCapacity() {
		return Integer.parseInt(myEventRecord.substring(142, 147));
	}

	private static String getEventType() {
		return myEventRecord.substring(128, 142).trim();
	}

	private static String getEndDateTime() {
		return myEventRecord.substring(113, 128);
	}

	private static String getStartDateTime() {
		return myEventRecord.substring(98, 113);
	}

	// Please don't change code in the EventData class without discussing with
	// the instructor.
	private static String getContactEmail() {
		return myEventRecord.substring(71, 98);
	}

	private static String getContactZIP() {
		return myEventRecord.substring(66, 71);
	}

	private static String getContactState() {
		return myEventRecord.substring(64, 66);
	}

	private static String getContactCity() {
		return myEventRecord.substring(49, 64);
	}

	private static String getContactAddress() {
		return myEventRecord.substring(35, 49);
	}

	private static String getContactPhone() {
		return myEventRecord.substring(25, 35);
	}

	private static String getContactName() {
		return myEventRecord.substring(10, 25);
	}

	private static String getEventName() {
		return myEventRecord.substring(0, 10);
	}

	// Please don't change code in the EventData class without discussing with
	// the instructor.

}
