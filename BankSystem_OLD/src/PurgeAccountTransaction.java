

public class PurgeAccountTransaction extends Transaction
{
    /**
     * Processes the purge transaction on the account
     */
    public boolean process()
    {
        boolean isTransactionComplete = isAccountFound( getFromAccount() );

        // Checks if the account is present or not
        if ( isTransactionComplete )
        {
            // Checks if the end balance of the account is zero or not
            if ( getFromAccount().getEndTtlBalance() == 0 )
            {
                // If the end balance is zero, retrieve the array list from the
                // bank system class
                BankSystem bank = BankSystem.getInstance();

                isTransactionComplete = bank.removeAccount( getFromAccount() );

                System.out.println( "Purged account "
                        + getFromAccount().getAccountNbr() );
            }
            else
            {
                // If the balance is not zero, displays the message cannot purge
                // account and updates the transaction complete variable to
                // false
                System.out.println( "Cannot purge account: "
                        + getTransactionAccountNbr() + " since the balance = "
                        + getFromAccount().getEndTtlBalance() + " is not zero" );
                isTransactionComplete = false;
            }
        }
        return isTransactionComplete;
    }

    public String transactionType()
    {
        return "Purge";
    }

}
