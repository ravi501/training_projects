public class MainApp
{
    public static void main( String[] args )
    {
        BankSystem bank = BankSystem.getInstance();
        bank.process();
    }

}
