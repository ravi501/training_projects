import java.util.Date;


public abstract class Transaction implements ITransaction
{
    // Transaction class contains the transaction information to be processed on
    // the individual accounts.

    // Stores the transaction date
    private Date      processDate;

    // Stores the transaction amount
    private double    amount;

    // Stores the transaction type specifying if it is a open account or closing
    // account or deposit or withdrawal type of transaction
    private char      type;

    // Stores the account object on which the transaction needs to be processed
    private Account   fromAccount;

    private Account   toAccount;

    // Stores the account number corresponding to the current transaction
    private String    transactionFromAccountNbr;

    // Constants for each transaction type
    final static char OPEN_ACCOUNT_TRANSACTION     = 'O';
    final static char CLOSE_ACCOUNT_TRANSACTION    = 'C';
    final static char DEPOSIT_ACCOUNT_TRANSACTION  = 'D';
    final static char WITHDRAW_ACCOUNT_TRANSACTION = 'W';
    final static char PURGE_ACCOUNT_TRANSACTION    = 'P';
    final static char TRANSFER_ACCOUNT_TRANSACTION = 'T';

    public abstract String transactionType();

    /**
     * Getter method for Transaction date
     * 
     * @return - Returns transaction date
     */
    public Date getProcessDate()
    {
        return processDate;
    }

    /**
     * Setter method for transaction date
     * 
     * @param processDate
     */
    public void setProcessDate( Date processDate )
    {
        this.processDate = processDate;
    }

    /**
     * Getter method for transaction amount
     * 
     * @return - Returns the transaction amount corresponding to the current
     *         transaction
     */
    public double getAmount()
    {
        return amount;
    }

    /**
     * Setter method for transaction amount
     * 
     * @param transactionAmount
     */
    public void setAmount( double transactionAmount )
    {
        this.amount = transactionAmount;
    }

    /**
     * Getter method for transaction type
     * 
     * @return - returns the type of the current transaction
     */
    public char getType()
    {
        return type;
    }

    /**
     * Setter method for setting the transaction type
     * 
     * @param transactionType
     */
    public void setType( char transactionType )
    {
        this.type = transactionType;
    }

    /**
     * Getter method for getting the account object
     * 
     * @return - Returns an account
     */
    public Account getFromAccount()
    {
        return fromAccount;
    }

    /**
     * Setter method for account object
     * 
     * @param account
     */
    public void setFromAccount( Account account )
    {
        this.fromAccount = account;
    }

    public Account getToAccount()
    {
        return toAccount;
    }

    public void setToAccount( Account toAccount )
    {
        this.toAccount = toAccount;
    }

    /**
     * Getter method for transaction account number
     * 
     * @return
     */
    public String getTransactionAccountNbr()
    {
        return transactionFromAccountNbr;
    }

    /**
     * Setter method for transaction account number
     * 
     * @param transactionAccountNbr
     */
    public void setTransactionAccountNbr( String transactionAccountNbr )
    {
        this.transactionFromAccountNbr = transactionAccountNbr;
    }

    /**
     * Method for initializing the members of the transaction object
     */
    public void build( TransactionParser transactionRow )
    {
        setAmount( transactionRow.getAmount() );
        setProcessDate( transactionRow.getProcessDate() );

        // Retrieves the account details from the populated accounts class by
        // using the account number from transaction record
        setFromAccount( BankSystem.getInstance().getAccount(
                transactionRow.getFromAccountNumber() ) );

        // Sets the to account field for transfer transaction
        setToAccount( BankSystem.getInstance().getAccount(
                transactionRow.getToAccountNumber() ) );

        // Sets the transaction account number
        setTransactionAccountNbr( transactionRow.getFromAccountNumber() );

        setType( transactionRow.getTransactionType() );
    }

    /**
     * This method processes the transaction on the corresponding accounts
     */
    public void processTransaction()
    {
        // This returns if the transaction has been successful or not
        boolean isTransactionSuccessful = process();

        // Check if the transaction is successful or not
        if ( isTransactionSuccessful )
        {
            // If the transaction is successful, increment the number of
            // transactions on the account by 1
            fromAccount.setTtlNbrOfTransactions( getFromAccount()
                    .getTtlNbrOfTransactions() + 1 );

            // If a transaction involves two accounts update the number of
            // transactions in the second account as well. Else ignore it.
            if ( toAccount != null )
            {
                toAccount.setTtlNbrOfTransactions( getToAccount()
                        .getTtlNbrOfTransactions() + 1 );
            }
        }

    }

    public boolean updateStatus( char status, Account account )
    {
        boolean updateStatus = true;
        if ( status == Account.ACCOUNT_ACTIVE_STATUS )
        {
            if ( account.getStatus() == Account.ACCOUNT_CLOSED_STATUS )
            {
                System.out.println( "Activating closed account "
                        + account.getAccountNbr() );
                account.setStatus( status );
            }
        }
        else if ( status == Account.ACCOUNT_CLOSED_STATUS )
        {
            if ( account.getEndTtlBalance() == 0 )
            {
                System.out.println( "Closing account "
                        + account.getAccountNbr() );
                account.setStatus( status );
            }
            else
            {
                updateStatus = false;
            }
        }
        return updateStatus;
    }

    public boolean isAccountFound( Account account )
    {
        boolean accountFound = true;

        if ( account == null )
        {
            System.out.println( transactionType()
                    + " transaction could not be processed because account:"
                    + getTransactionAccountNbr() + " not found " );
            accountFound = false;
        }
        return accountFound;
    }

    /**
     * Gets the object for the corresponding transaction type
     * 
     * @param tp
     *            - Takes as input the transaction parser
     * @return - Returns the object of the specific transaction type
     */
    public static Transaction getTransaction( TransactionParser tp )
    {
        Transaction transaction = null;

        switch ( tp.getTransactionType() )
        {
        // Deposit Transaction
            case DEPOSIT_ACCOUNT_TRANSACTION:
            {
                transaction = new DepositTransaction();
                break;
            }
            // Withdrawal Transaction
            case WITHDRAW_ACCOUNT_TRANSACTION:
            {
                transaction = new WithdrawalTransaction();
                break;
            }
            // Open Transaction
            case OPEN_ACCOUNT_TRANSACTION:
            {
                transaction = new OpenAccountTransaction();
                break;
            }
            // Close Transaction
            case CLOSE_ACCOUNT_TRANSACTION:
            {
                transaction = new CloseAccountTransaction();
                break;
            }
            // Purge transaction
            case PURGE_ACCOUNT_TRANSACTION:
            {
                transaction = new PurgeAccountTransaction();
                break;
            }
            // Transfer transaction
            case TRANSFER_ACCOUNT_TRANSACTION:
            {
                transaction = new TransferTransaction();
                break;
            }

            default:
            {
                System.out.println( "Invalid transaction type: "
                        + tp.getTransactionType() + " from "
                        + tp.getFromAccountNumber() + " to "
                        + tp.getToAccountNumber() + " accounts" );
            }
        }
        return transaction;
    }
}
