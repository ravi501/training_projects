public class TransferTransaction extends Transaction
{
    // Method processes the transfer transaction
    public boolean process()
    {
        boolean isTransactionComplete = isAccountFound( getFromAccount() )
                && isAccountFound( getToAccount() );

        // If both the from and to accounts are present in the system, process
        // the transaction
        if ( isTransactionComplete )
        {
            // If transaction amount is less than final balance in from account,
            // process the transaction
            if ( getAmount() <= getFromAccount().getEndTtlBalance() )
            {
                // Reduce the balance from the sender account
                getFromAccount().setEndTtlBalance(
                        getFromAccount().getEndTtlBalance() - getAmount() );

                // Increase the balance in the receiver account
                getToAccount().setEndTtlBalance(
                        getToAccount().getEndTtlBalance() + getAmount() );

                // Set the credit account status
                updateStatus( Account.ACCOUNT_ACTIVE_STATUS, getToAccount() );
            }
            else
            {
                // If the transaction amount is greater than final balance,
                // display the error message
                System.out.println( "Insufficient funds in account "
                        + getFromAccount().getAccountNbr() + " to transfer" );
                isTransactionComplete = false;
            }
        }
        return isTransactionComplete;
    }

    public String transactionType()
    {
        return "Transfer";
    }
}
