public class WithdrawalTransaction extends Transaction
{
    /**
     * Method is used to withdraw amount from the account
     */
    public boolean process()
    {
        boolean isTransactionComplete = isAccountFound( getFromAccount() );

        // Checks if the account is present or not
        if ( isTransactionComplete )
        {

            // If the transaction amount is less than or equals account balance,
            // process transaction
            if ( getAmount() <= getFromAccount().getEndTtlBalance() )
            {
                // Withdraw amount and reduce the final balance from the account
                getFromAccount().setEndTtlBalance(
                        getFromAccount().getEndTtlBalance() - getAmount() );

                // If the transaction amount is same as the final balance of the
                // account, close the account
                updateStatus( Account.ACCOUNT_CLOSED_STATUS, getFromAccount() );
            }
            else
            {
                System.out.println( "Transaction amount " + getAmount()
                        + " exceeds the available balance "
                        + getFromAccount().getEndTtlBalance() );
            }
        }
        return isTransactionComplete;
    }

    public String transactionType()
    {
        return "Withdraw";
    }
}
