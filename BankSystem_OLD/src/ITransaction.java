
public interface ITransaction
{
    boolean process();

    void build( TransactionParser tp );
}
