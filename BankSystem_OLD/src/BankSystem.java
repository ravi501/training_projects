import java.text.DecimalFormat;
import java.util.ArrayList;


public class BankSystem
{
    // The Bank system maintains the accounts and also processes the
    // transactions on the accounts

    // accountArray stores the list of all the accounts present in the system
    private ArrayList< Account > accountArray        = new ArrayList< Account >();

    // Account file - To handle opening the accounts file
    private FileUtility          accountFile         = new FileUtility();
    private String               accountFileName     = "Master.txt";

    // Transaction file - To handle opening the transactions file
    private FileUtility          transactionFile     = new FileUtility();
    private String               transactionFileName = "Trans.txt";

    // New Master file for storing the updated accounts information
    private FileUtility          newMasterFile       = new FileUtility();
    private String               newMasterFileName   = "NewMaster.txt";

    // A static instance of the BankSystem implemented using Singleton Design
    // Pattern
    private static BankSystem    instance;

    /**
     * Constructor to initialize the Bank system
     */
    private BankSystem()
    {

    }

    /**
     * Creates only a single instance of the Bank system
     * 
     * @return BankSystem - returns a instance of Bank system
     */
    public static BankSystem getInstance()
    {
        if ( instance == null )
        {
            instance = new BankSystem();
        }
        return instance;
    }

    /**
     * Getter method for Account Array
     * 
     * @return ArrayList< Account > - Returns an Array of Accounts
     */
    public ArrayList< Account > getAccountArray()
    {
        return accountArray;
    }

    /**
     * Setter method for Account Array
     * 
     * @param accountArray
     *            - Sets the account array
     */
    public void setAccountArray( ArrayList< Account > accountArray )
    {
        this.accountArray = accountArray;
    }

    /**
     * Opens the account and transaction files, loads the account array and
     * initializes the processing of the transactions
     */
    public void process()
    {
        accountFile.openReadFile( accountFileName );
        loadAccountArray();
        transactionFile.openReadFile( transactionFileName );
        processTransactions();
        printAccounts();
        writeNewMasterFile();
    }

    /**
     * Writes to the new master file
     */
    private void writeNewMasterFile()
    {
        ArrayList< String > accountArrayString = new ArrayList< String >();

        // Iterate through the account array list
        for ( int i = 0; i < accountArray.size(); i++ )
        {
            //
            String accountString = new String( accountArray.get( i )
                    .getAccountNbr() );

            String a = "" + accountArray.get( i ).getStatus();

            String pattern = "000000000000000.######";
            DecimalFormat decimalFormat = new DecimalFormat( pattern );

            accountString += a
                    + decimalFormat.format( accountArray.get( i )
                            .getEndTtlBalance() );

            accountArrayString.add( accountString );
        }
        newMasterFile.writeFile( newMasterFileName, accountArrayString );
    }

    /**
     * Loads each incoming account record into the array of accounts
     */
    private void loadAccountArray()
    {
        String accountData = accountFile.getNextLine();

        while ( accountData != null )
        {
            AccountParser accountParser = new AccountParser( accountData );
            Account account = new Account();
            account.build( accountParser );
            accountArray.add( account );
            accountData = accountFile.getNextLine();
        }
    }

    /**
     * Reads each transaction entry from the transaction file and processes the
     * read transaction entry
     */
    private void processTransactions()
    {
        // Reads the next line from the transaction entry
        String transactionData = transactionFile.getNextLine();

        // Condition to check if there is an incoming transaction entry
        while ( transactionData != null )
        {
            // Parses the input file into transaction parser and subsequently
            // into a transaction object
            TransactionParser tp = new TransactionParser( transactionData );
            Transaction transaction = Transaction.getTransaction( tp );

            // Builds the transaction object and processes the transaction
            if ( transaction != null )
            {
                transaction.build( tp );
                transaction.processTransaction();
            }

            // Reads the next transaction record from the transaction file
            transactionData = transactionFile.getNextLine();
        }
    }

    /**
     * Retrieves a account record from the array of accounts
     * 
     * @param accountNumber
     *            - Takes an account number as input string
     * @return - Returns the Account object corresponding to the requested
     *         account number
     */
    public Account getAccount( String accountNumber )
    {
        Account account = null;

        // Iterates through the account array and reads each account record in
        // the account array
        for ( int i = 0; i < accountArray.size(); i++ )
        {
            Account bankAccount = accountArray.get( i );

            if ( bankAccount.getAccountNbr().equals( accountNumber ) )
            {
                account = bankAccount;
                break;
            }
        }
        return account;
    }

    public boolean addNewAccount( Account account )
    {
        boolean isAccountadded = getAccountArray().add( account );

        return isAccountadded;
    }

    public boolean removeAccount( Account account )
    {
        boolean isAccountRemoved = getAccountArray().remove( account );

        return isAccountRemoved;
    }

    /**
     * Prints the individual account records
     */
    private void printAccounts()
    {
        for ( int i = 0; i < accountArray.size(); i++ )
        {
            Account account = accountArray.get( i );
            account.print();
        }
    }
}
