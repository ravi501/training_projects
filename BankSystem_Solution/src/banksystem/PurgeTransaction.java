package banksystem;
public class PurgeTransaction extends Transaction
{
    public boolean process()
    {
        boolean transProcessed = false;
        Account myAccount = getFromAccount();

        if ( myAccount != null )
        {
            if ( myAccount.getBalance() == 0 )
            {
                Bank.getInstance().removeAccount( myAccount );

                transProcessed = true;
            }
            else
            {
                StringBuilder builder = new StringBuilder();
                builder.append( "Account cannot be purged - balance > zero. Acct#: " );
                builder.append( myAccount.getAccountNumber() );

                /*
                 * System.out .println(
                 * "Account cannot be purged - balance > zero. Acct#: " +
                 * myAccount.getAccountNumber() );
                 */
            }
        }

        return transProcessed;
    }
}
