package banksystem;
public class CloseTransaction extends Transaction
{

    public boolean process()
    {
        boolean transProcessed = false;
        Account myAccount = getFromAccount();

        if ( myAccount != null )
        {
            if ( myAccount.getBalance() == 0 )
            {
                myAccount.setAccountStatus( 'C' );
                transProcessed = true;
            }
            else
            {
                StringBuilder builder = new StringBuilder();
                builder.append( "Account cannot be closed - balance > zero: " );
                builder.append( " Trans Acct#: " );
                builder.append( getFromAccountNumber() );

                System.out.println( builder.toString() );

                /*
                 * System.out .println(
                 * "Account cannot be closed - balance > zero: " +
                 * " Trans Acct#: " + getFromAccountNumber() );
                 */
            }
        }

        return transProcessed;
    }
}
