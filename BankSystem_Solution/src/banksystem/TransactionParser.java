package banksystem;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class TransactionParser
{
    String line = null;

    public TransactionParser( String fileLine )
    {
        this.line = fileLine;
    }

    public char getTransactionType()
    {
        return line.substring( 11, 12 ).toUpperCase().charAt( 0 );
    }

    public String getFromAccountNumber()
    {
        return line.substring( 0, 11 );
    }

    public String getToAccountNumber()
    {
        return line.substring( 35, 46 );
    }

    public Date getProcessDate()
    {
        Date result = null;
        DateFormat df = new SimpleDateFormat( "yyyy-MM-dd" );

        try
        {
            result = df.parse( line.substring( 25, 35 ) );
        }
        catch ( ParseException e )
        {
            e.printStackTrace();
        }

        return result;
    }

    public double getAmount()
    {
        double tempAmount = 0;
        
        // This code is dividing the amount by 100 to allow for 2 digits in the 
        // text transaction file.
        tempAmount = Double.parseDouble( line.substring( 12, 25 ) );
        return tempAmount / 100;
        
    }

}
