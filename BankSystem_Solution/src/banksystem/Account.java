package banksystem;
public class Account {
	private String accountNumber;
	private char accountStatus;
	private double balance;
	private double begBalance;

	public Account() {

	}

	public Account(String accountNumber, char accountStatus, double balance,
			double begBalance) {
		this.accountNumber = accountNumber;
		this.accountStatus = accountStatus;
		this.balance = balance;
		this.begBalance = begBalance;
	}

	public void loadAccountObject(AccountParser ap) {
		setAccountNumber(ap.getAccountNumber());
		setAccountStatus(ap.getStatus());
		setBalance(ap.getBalance());
		setBegBalance(ap.getBalance());
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public char getAccountStatus() {
		return accountStatus;
	}

	public double getBalance() {
		return balance;
	}

	public double getBegBalance() {
		return begBalance;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setAccountStatus(char accountStatus) {
		this.accountStatus = accountStatus;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void setBegBalance(double begBalance) {
		this.begBalance = begBalance;
	}

}
