package banksystem;
public class AccountParser {
	String accountRecord = null;

	public AccountParser(String accountRecord) {
		this.accountRecord = accountRecord;
	}

	public String getAccountNumber() {
		return accountRecord.substring(0, 11);
	}

	public char getStatus() {
		return accountRecord.substring(11, 12).charAt(0);
	}

	public double getBalance() {
		return (Double.parseDouble(accountRecord.substring(12, 27))) / 100;
	}
}
