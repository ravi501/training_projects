package banksystem;
public class TransferTransaction extends Transaction
{
    public boolean process()
    {
        boolean transProcessed = false;
        Account myFromAccount = getFromAccount();
        Account myToAccount = getToAccount();

        if ( myFromAccount != null )
        {
            if ( myToAccount != null )
            {
                // Withdraw from 'From' account
                // Deposit to 'To' account
                transProcessed = true;
            }
            else
            {
                StringBuilder builder = new StringBuilder();
                builder.append( "Transfer cannot be processed - 'To' account cannot be found. To Acct#: " );
                builder.append( myFromAccount.getAccountNumber() );

                System.out.println( builder.toString() );

                /*
                 * System.out .println(
                 * "Transfer cannot be processed - 'To' account cannot be found. To Acct#: "
                 * + myFromAccount.getAccountNumber() );
                 */
            }
        }
        else
        {
            StringBuilder builder = new StringBuilder();
            builder.append( "Transfer cannot be processed - 'From' account cannot be found. From Acct#: " );
            builder.append( myToAccount.getAccountNumber() );

            System.out.println( builder.toString() );

            /*
             * System.out .println(
             * "Transfer cannot be processed - 'From' account cannot be found. From Acct#: "
             * + myToAccount.getAccountNumber() );
             */
        }

        return transProcessed;
    }
}
