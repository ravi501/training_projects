package banksystem;
public class WithdrawalTransaction extends Transaction
{
    public boolean process()
    {
        boolean transProcessed = false;
        Account myAccount = getFromAccount();

        if ( myAccount != null )
        {
            if ( getTransAmount() <= myAccount.getBalance() )
            {
                myAccount
                        .setBalance( myAccount.getBalance() - getTransAmount() );

                if ( myAccount.getBalance() == 0 )
                {
                    myAccount.setAccountStatus( 'C' );
                }
                transProcessed = true;
            }
            else
            {
                StringBuilder builder = new StringBuilder();
                builder.append( "Insufficient balance for withdrawal. Acct#: " );
                builder.append( myAccount.getAccountNumber() );

                System.out.println( builder.toString() );

                /*
                 * System.out .println(
                 * "Insufficient balance for withdrawal. Acct#: " +
                 * myAccount.getAccountNumber() );
                 */
            }
        }

        return transProcessed;
    }
}
