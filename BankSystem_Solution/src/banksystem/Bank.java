
package banksystem;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;


public class Bank
{
    private static Bank          instance;

    private ArrayList< Account > accountArray     = new ArrayList< Account >();

    private Transaction[]        transactionArray = new Transaction[ 11 ];

    private String               accountFileName  = "Master.txt";
    private String               transFileName    = "Trans.txt";

    private FileUtility          accountFile      = new FileUtility();
    private FileUtility          transFile        = new FileUtility();

    private Bank()
    {
    }

    public static Bank getInstance()
    {
        if ( instance == null )
        {
            instance = new Bank();
        }

        return instance;
    }

    public void process()
    {
        accountFile.openReadFile( accountFileName );

        loadAccountArray();

        transFile.openReadFile( transFileName );
        storeAndProcessTransactions();

        printSummary();
    }

    private void loadAccountArray()
    {
        String accountRecord = accountFile.getNextLine();

        while ( accountRecord != null )
        {
            AccountParser ap = new AccountParser( accountRecord );
            Account account = new Account();

            account.loadAccountObject( ap );
            accountArray.add( account );

            accountRecord = accountFile.getNextLine();
        }
    }

    private void storeAndProcessTransactions()
    {
        String transRecord = transFile.getNextLine();
        int counter = 0;

        while ( transRecord != null )
        {
            TransactionParser tp = new TransactionParser( transRecord );
            Transaction trans = Transaction.createNewTransaction( tp );

            if ( trans != null )
            {
                trans.build( tp );
                transactionArray[ counter ] = trans;
                trans.process();
                counter++;
            }

            transRecord = transFile.getNextLine();
        }
    }

    public Account searchForAccount( String accountNumber )
    {
        Account foundAccount = null;

        for ( int i = 0; i < accountArray.size(); i++ )
        {
            Account tempAccount = accountArray.get( i );

            if ( accountNumber.equals( tempAccount.getAccountNumber() ) )
            {
                foundAccount = tempAccount;
                break;
            }
        }
        return foundAccount;
    }

    public Account addNewAccount()
    {
        Account account = new Account();
        accountArray.add( account );

        return account;
    }

    public void removeAccount( Account account )
    {
        accountArray.remove( account );
    }

    private void printSummary()
    {
        String pattern = "0000000000.00";
        DecimalFormat decimalFormat = new DecimalFormat( pattern );

        NumberFormat numberFormatter = NumberFormat.getCurrencyInstance();
        // System.out.println(formatter.format(amt));

        System.out
                .println( "\n*****************************************************" );
        System.out
                .println( "*                    Master Accounts                *" );
        System.out
                .println( "*****************************************************" );

        for ( int i = 0; i < accountArray.size(); i++ )
        {
            StringBuilder builder = new StringBuilder();
            builder.append( "Account#: " );
            builder.append( accountArray.get( i ).getAccountNumber() );
            builder.append( "   Status: " );
            builder.append( accountArray.get( i ).getAccountStatus() );
            builder.append( "   Beg: " );
            builder.append( numberFormatter.format( accountArray.get( i )
                    .getBegBalance() ) );
            builder.append( "   End: " );
            builder.append( numberFormatter.format( accountArray.get( i )
                    .getBalance() ) );

            System.out.println( builder.toString() );

            /*
             * System.out.println( "Account#: " + accountArray.get( i
             * ).getAccountNumber() + "   Status: " + accountArray.get( i
             * ).getAccountStatus() + "   Beg: " + numberFormatter.format(
             * accountArray.get( i ) .getBegBalance() ) + "   End: " +
             * numberFormatter.format( accountArray.get( i ) .getBalance() ) );
             */
        }
    }
}
