package banksystem;
public class DepositTransaction extends Transaction {
	public boolean process() {
		boolean transProcessed = false;
		Account myAccount = getFromAccount();

		if (myAccount != null) {
			myAccount.setBalance(myAccount.getBalance() + getTransAmount());
			transProcessed = true;
		}

		return transProcessed;
	}
}
