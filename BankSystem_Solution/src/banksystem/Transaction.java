package banksystem;
import java.util.Date;


public abstract class Transaction implements ITransaction
{
    private double  transAmount;
    private Date    processDate;
    private Account fromAccount;
    private Account toAccount;
    private String  fromAccountNumber;

    public static Transaction createNewTransaction( TransactionParser tp )
    {

        Transaction tempTrans = null;

        switch ( tp.getTransactionType() )
        {
            case 'O':
            {
                tempTrans = new OpenTransaction();
                break;
            }
            case 'C':
            {
                tempTrans = new CloseTransaction();
                break;
            }
            case 'W':
            {
                tempTrans = new WithdrawalTransaction();
                break;
            }
            case 'D':
            {
                tempTrans = new DepositTransaction();
                break;
            }
            case 'P':
            {
                tempTrans = new PurgeTransaction();
                break;
            }
            case 'T':
            {
                tempTrans = new TransferTransaction();
                break;
            }
            default:
            {
                StringBuilder builder = new StringBuilder();
                builder.append( "Invalid transaction type. Acct#:" );
                builder.append( tp.getFromAccountNumber() );
                builder.append( "Type:" );
                builder.append( tp.getTransactionType() );
                System.out.println( builder.toString() );

                break;
            }
        }

        return tempTrans;
    }

    public void build( TransactionParser tp )
    {
        char tempType = tp.getTransactionType();
        Account tempAccount;

        tempAccount = retrieveAccountFromBank( tp.getFromAccountNumber() );

        if ( tempAccount != null )
        {
            setFromAccount( tempAccount );
            if ( tempAccount.getAccountStatus() == 'C' )
            {
                StringBuilder builder = new StringBuilder();
                builder.append( "Account is Closed - account set to Active. Acct#: " );
                builder.append( tempAccount.getAccountNumber() );
                builder.append( "   Type: " );
                builder.append( tempType );
                System.out.println( builder.toString() );

                /*
                 * System.out .println(
                 * "Account is Closed - account set to Active. Acct#: " +
                 * tempAccount.getAccountNumber() + "   Type: " + tempType );
                 */

                tempAccount.setAccountStatus( 'A' );
            }
        }
        else
        {
            if ( tempType != 'O' )
            {
                StringBuilder builder = new StringBuilder();
                builder.append( "Account not found for transaction. Acct#: " );
                builder.append( tp.getFromAccountNumber() );
                builder.append( "   Type: " );
                builder.append( tempType );

                System.out.println( builder.toString() );

                /*
                 * System.out .println(
                 * "Account not found for transaction. Acct#: " +
                 * tp.getFromAccountNumber() + "   Type: " + tempType );
                 */
            }
        }

        setTransAmount( tp.getAmount() );
        setProcessDate( tp.getProcessDate() );
        setFromAccountNumber( tp.getFromAccountNumber() );

        if ( tempType == 'T' )
        {
            tempAccount = retrieveAccountFromBank( tp.getToAccountNumber() );

            if ( tempAccount != null )
            {
                setToAccount( tempAccount );
                if ( tempAccount.getAccountStatus() == 'C' )
                {
                    StringBuilder builder = new StringBuilder();
                    builder.append( "Account is Closed - account set to Active. Acct#: " );
                    builder.append( tempAccount.getAccountNumber() );
                    builder.append( "   Type: " );
                    builder.append( tempType );
                    System.out.println( builder.toString() );

                    /*
                     * System.out .println(
                     * "Account is Closed - account set to Active. Acct#: " +
                     * tempAccount.getAccountNumber() + "   Type: " + tempType
                     * );
                     */
                    tempAccount.setAccountStatus( 'A' );
                }
            }
            else
            {
                if ( tempType != 'O' )
                {
                    StringBuilder builder = new StringBuilder();
                    builder.append( "Account not found for transaction. Acct#: " );
                    builder.append( tp.getToAccountNumber() );
                    builder.append( "   Type: " );
                    builder.append( tempType );

                    System.out.println( builder.toString() );

                    /*
                     * System.out .println(
                     * "Account not found for transaction. Acct#: " +
                     * tp.getToAccountNumber() + "   Type: " + tempType );
                     */
                }
            }
        }

    }

    public Account retrieveAccountFromBank( String accountNumber )
    {
        return Bank.getInstance().searchForAccount( accountNumber );
    }

    public Account getFromAccount()
    {
        return fromAccount;
    }

    public double getTransAmount()
    {
        return transAmount;
    }

    public Date getProcessDate()
    {
        return processDate;
    }

    public Account getToAccount()
    {
        return toAccount;
    }

    public void setFromAccount( Account fromAccount )
    {
        this.fromAccount = fromAccount;
    }

    public void setTransAmount( double transAmount )
    {
        this.transAmount = transAmount;
    }

    public void setProcessDate( Date processDate )
    {
        this.processDate = processDate;
    }

    public void setToAccount( Account toAccount )
    {
        this.toAccount = toAccount;
    }

    public String getFromAccountNumber()
    {
        return fromAccountNumber;
    }

    public void setFromAccountNumber( String fromAccountNumber )
    {
        this.fromAccountNumber = fromAccountNumber;
    }

}
