package banksystem;
public class OpenTransaction extends Transaction
{
    public boolean process()
    {
        boolean transProcessed = false;

        if ( getFromAccount() == null )
        {
            Account myAccount = Bank.getInstance().addNewAccount();

            myAccount.setAccountNumber( getFromAccountNumber() );
            myAccount.setAccountStatus( 'A' );
            myAccount.setBalance( getTransAmount() );
            myAccount.setBegBalance( getTransAmount() );

            transProcessed = true;
        }
        else
        {
            StringBuilder builder = new StringBuilder();
            builder.append( "Account cannot be opened - account number already exists. Acct#: " );
            builder.append( getFromAccountNumber() );

            System.out.println( builder.toString() );

            /*
             * System.out .println(
             * "Account cannot be opened - account number already exists. Acct#: "
             * + getFromAccountNumber() );
             */
        }

        return transProcessed;
    }
}
