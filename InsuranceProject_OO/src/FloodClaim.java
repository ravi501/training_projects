public class FloodClaim extends Claim
{
    public final int FLOOD_DEDUCTIBLE = 2500;

    public void adjustClaim()
    {
        setAmount( getAmount() - FLOOD_DEDUCTIBLE );
    }
}
