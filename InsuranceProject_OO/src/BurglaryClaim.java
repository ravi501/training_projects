public class BurglaryClaim extends Claim
{
    public final int MAX_BURGLARY_CLAIM  = 10000;

    public final int BURGLARY_DEDUCTIBLE = 500;

    public void adjustClaim()
    {
        if ( getAmount() > MAX_BURGLARY_CLAIM )
        {
            setAmount( MAX_BURGLARY_CLAIM );
        }
        setAmount( getAmount() - BURGLARY_DEDUCTIBLE );
    }
}
