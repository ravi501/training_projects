public class AccidentClaim extends Claim
{
    public final int MAX_ACCIDENT_CLAIM  = 2500;

    public final int ACCIDENT_DEDUCTIBLE = 250;

    public void adjustClaim()
    {
        if ( getAmount() > MAX_ACCIDENT_CLAIM )
        {
            setAmount( MAX_ACCIDENT_CLAIM );
        }
        setAmount( getAmount() - ACCIDENT_DEDUCTIBLE );
    }
}
