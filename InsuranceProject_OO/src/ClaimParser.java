
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ClaimParser
{
    String claimLine = null;

    public ClaimParser( String fileLine )
    {
        this.claimLine = fileLine;
    }

    public char getClaimType()
    {
        return claimLine.substring( 15 ).charAt( 0 );
    }

    public String getPolicyNumber()
    {
        return claimLine.substring( 0, 15 );
    }

    public Date getClaimDate()
    {
        Date result = null;
        DateFormat df = new SimpleDateFormat( "yyyy-MM-dd" );

        try
        {
            result = df.parse( claimLine.substring( 16, 26 ) );
        }
        catch ( ParseException e )
        {
            e.printStackTrace();
        }

        return result;
    }

    public double getAmount()
    {
        double claimAmt = 0.0;

        try
        {
            claimAmt = (Double.parseDouble( claimLine.substring( 26, 37 ) ) / 100);
        }
        catch ( ArithmeticException e )
        {
            System.out.println( "ERROR OCCURRED PARSING claimAmt" );
            e.printStackTrace();
        }

        return claimAmt;
    }

}
