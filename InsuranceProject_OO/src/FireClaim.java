public class FireClaim extends Claim
{
    public final int FIRE_DEDUCTIBLE = 1000;

    public void adjustClaim()
    {
        setAmount( getAmount() - FIRE_DEDUCTIBLE );
    }
}
