public class PurchaseParser
{
    String purchaseLine = null;

    public PurchaseParser( String fileLine )
    {
        this.purchaseLine = fileLine;
    }

    public String getPurchaseNumber()
    {
        return purchaseLine.substring( 0, 15 );
    }

    public int getNbrOfDiscounts()
    {
        int nbrOfDiscounts = 0;

        try
        {
            nbrOfDiscounts = Integer.parseInt( purchaseLine.substring( 15, 17 ) );
        }
        catch ( ArithmeticException e )
        {
            System.out.println( "ERROR OCCURRED PARSING nbrOfDiscounts" );
            e.printStackTrace();
        }

        return nbrOfDiscounts;
    }

    public double getAmtOfDiscounts()
    {
        double amtOfDiscounts = 0.0;

        try
        {
            amtOfDiscounts = (Double
                    .parseDouble( purchaseLine.substring( 17, 28 ) ) / 100);
        }
        catch ( ArithmeticException e )
        {
            System.out.println( "ERROR OCCURRED PARSING amtOfDiscounts" );
            e.printStackTrace();
        }

        return amtOfDiscounts;
    }
}
