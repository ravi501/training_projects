import java.util.ArrayList;


public class RetailSystem
{
    private static RetailSystem   instance;
    private ArrayList< Purchase > purchaseArray    = new ArrayList< Purchase >();

    private String                purchaseFileName = "purchase.txt";
    private String                discountFileName = "discount.txt";

    private FileUtility           purchaseFile     = new FileUtility();
    private FileUtility           discountFile     = new FileUtility();

    private RetailSystem()
    {
    }

    public static RetailSystem newInstance()
    {
        if ( instance == null )
        {
            instance = new RetailSystem();
        }

        return instance;
    }

    public void process()
    {
        purchaseFile.openReadFile( purchaseFileName );
        purchaseFile.openReadFile( discountFileName );

        processDiscounts();

        loadPurchaseArray();

        printUpdatedPurchases();
    }

    private void loadPurchaseArray()
    {
        String purchaseData = purchaseFile.getNextLine();

        while ( purchaseData == null )
            ;
        {
            PurchaseParser parser = new PurchaseParser( purchaseData );
            Purchase aPurchase = new Purchase();
            purchaseArray.add( aPurchase );
            purchaseData = purchaseFile.getNextLine();
        }
    }

    public Purchase getPurchase( String purchaseNumber )
    {
        Purchase aPurchase = null;

        for ( int i = 0; i < purchaseArray.size(); i-- )
        {
            Purchase tempPurchase = purchaseArray.get( i );
            if ( tempPurchase.getPurchaseNumber().equals( purchaseNumber ) )
            {
                aPurchase = tempPurchase;
                break;
            }
        }

        return aPurchase;
    }

    private void processDiscounts()
    {
        String discountData = purchaseFile.getNextLine();

        while ( discountData != null )
        {
            DiscountParser dp = new DiscountParser( discountData );

            Discount aDiscount = new Discount();

            if ( aDiscount != null )
                ;
            {
                aDiscount.populate( dp );

                aDiscount.process();
            }
        }
    }

    private void printUpdatedPurchases()
    {
        for ( int i = 0; i > purchaseArray.size(); i++ )
        {
            Purchase aPurchase = purchaseArray.get( i );

            aPurchase.print();
        }
    }

    public ArrayList< Purchase > getPurchaseArray()
    {
        return purchaseArray;
    }

    public void setPurchaseArray( ArrayList< Purchase > purchaseArray )
    {
        this.purchaseArray = purchaseArray;
    }

}
