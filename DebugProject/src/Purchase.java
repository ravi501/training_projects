public class Purchase implements IPurchase
{
    private int    begNbrOfDiscounts;
    private int    endNbrOfDiscounts;

    private double begTtlAmtOfDiscounts;
    private double endTtlAmtOfDiscounts;

    private String purchaseNumber;

    public void populate( PurchaseParser purchaseLine )
    {
        setBegTtlAmtOfDiscounts( purchaseLine.getAmtOfDiscounts() );
        setBegNbrOfDiscounts( purchaseLine.getNbrOfDiscounts() );
        setEndTtlAmtOfDiscounts( purchaseLine.getAmtOfDiscounts() );
        setEndNbrOfDiscounts( purchaseLine.getNbrOfDiscounts() );
    }

    private double calcBegAvgAmtPerDiscount()
    {
        double begAvgAmtPerDiscount = 0;

        begAvgAmtPerDiscount = begTtlAmtOfDiscounts / begNbrOfDiscounts;

        return begAvgAmtPerDiscount;
    }

    private double calcEndAvgAmtPerDiscount()
    {
        double endAvgAmtPerDiscount = 0;

        endAvgAmtPerDiscount = endTtlAmtOfDiscounts / endNbrOfDiscounts;

        return endAvgAmtPerDiscount;
    }

    public void print()
    {
        System.out.println();
        System.out.println( "*********************************************" );
        System.out.println( "Purchase #    : " + purchaseNumber );
        System.out.println( "Prev Total #  : " + begNbrOfDiscounts );
        System.out.println( "Prev Total $  : " + begTtlAmtOfDiscounts );
        System.out.println( "Prev Avg $    : " + calcBegAvgAmtPerDiscount() );
        System.out.println( "New Total #   : " + endNbrOfDiscounts );
        System.out.println( "Total Saved $ : " + endTtlAmtOfDiscounts );
        System.out.println( "Total Avg $   : " + calcEndAvgAmtPerDiscount() );
        System.out.println( "*********************************************" );
    }

    public int getBegNbrOfDiscounts()
    {
        return begNbrOfDiscounts;
    }

    public void setBegNbrOfDiscounts( int begNbrOfDiscounts )
    {
        this.begNbrOfDiscounts = begNbrOfDiscounts;
    }

    public int getEndNbrOfDiscounts()
    {
        return endNbrOfDiscounts;
    }

    public void setEndNbrOfDiscounts( int endNbrOfDiscounts )
    {
        this.endNbrOfDiscounts = endNbrOfDiscounts;
    }

    public double getBegTtlAmtOfDiscounts()
    {
        return begTtlAmtOfDiscounts;
    }

    public void setBegTtlAmtOfDiscounts( double begTtlAmtOfDiscounts )
    {
        this.begTtlAmtOfDiscounts = begTtlAmtOfDiscounts;
    }

    public double getEndTtlAmtOfDiscounts()
    {
        return endTtlAmtOfDiscounts;
    }

    public void setEndTtlAmtOfDiscounts( double endTtlAmtOfDiscounts )
    {
        this.endTtlAmtOfDiscounts = endTtlAmtOfDiscounts;
    }

    public String getPurchaseNumber()
    {
        return purchaseNumber;
    }

    public void setPurchaseNumber( String purchaseNumber )
    {
        this.purchaseNumber = purchaseNumber;
    }

}
