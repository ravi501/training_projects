public class StartingApp
{
    public static void main( String[] args )
    {
        System.out.println( "...Starting Retail System..." );
        System.out.println();

        RetailSystem is = RetailSystem.newInstance();
        is.process();

        System.out.println();
        System.out.println( "...Thank you for using the Retail System..." );
    }
}
