public class PriceErrorDiscount extends Discount
{
    public final int MAX_ERROR_DISCOUNT = 1000;

    public void adjustDiscount()
    {
        if ( getAmount() > MAX_ERROR_DISCOUNT )
        {
            setAmount( MAX_ERROR_DISCOUNT );
        }
    }
}
