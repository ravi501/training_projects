public class SeniorDiscount extends Discount
{
    public final int MAX_SENIOR_DISCOUNT = 50;

    public void adjustDiscount()
    {
        if ( getAmount() > MAX_SENIOR_DISCOUNT )
        {
            setAmount( MAX_SENIOR_DISCOUNT );
        }
    }
}
