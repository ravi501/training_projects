public class ManufacturerDiscount extends Discount
{
    public final int MAX_MANUFACTURER_DISCOUNT = 5;

    public void adjustDiscount()
    {
        if ( getAmount() >= MAX_MANUFACTURER_DISCOUNT )
        {
            setAmount( MAX_MANUFACTURER_DISCOUNT );
        }
    }
}
