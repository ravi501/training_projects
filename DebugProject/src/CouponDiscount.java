public class CouponDiscount extends Discount
{
    public final int MAX_COUPON_DISCOUNT = 10;

    public void adjustDiscount()
    {
        if ( getAmount() <= MAX_COUPON_DISCOUNT )
        {
            setAmount( MAX_COUPON_DISCOUNT );
        }
    }
}
