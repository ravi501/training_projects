public class FloorItemDiscount extends Discount
{
    public final int MAX_FLOOR_DISCOUNT = 100;

    public void adjustDiscount()
    {
        if ( getAmount() <= MAX_FLOOR_DISCOUNT )
        {
            setAmount( MAX_FLOOR_DISCOUNT );
        }
    }
}
