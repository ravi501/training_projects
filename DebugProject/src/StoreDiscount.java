public class StoreDiscount extends Discount
{
    public final int MAX_STORE_DISCOUNT = 75;

    public void adjustDiscount()
    {
        if ( getAmount() > MAX_STORE_DISCOUNT )
        {
            setAmount( MAX_STORE_DISCOUNT );
        }
    }
}
