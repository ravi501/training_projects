import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class FileUtility
{
    private BufferedReader reader = null;

    public void openReadFile( String fileName )
    {
        FileReader input;

        try
        {
            input = new FileReader( fileName );
            reader = new BufferedReader( input );
        }
        catch ( FileNotFoundException e )
        {
            System.out.println( "OPEN OF FILE FAILED - " + fileName );
            e.printStackTrace();
        }
    }

    public String getNextLine()
    {
        String result = null;

        if ( reader != null )
        {
            try
            {
                result = reader.readLine();

                if ( result != null )
                {
                    result = result.trim();
                }
                else
                {
                    reader.close();
                }
            }
            catch ( IOException e )
            {
                System.out.println( "ERROR OCCURRED READING NEXT LINE IN FILE" );
                e.printStackTrace();
            }
        }

        return result;
    }

}
