import java.util.Date;


public class Discount implements IDiscount
{
    private double   amount;
    private Date     discountDate;
    private char     discountType;
    private Purchase aPurchase;

    public void populate( DiscountParser discountLine )
    {
        setDiscountDate( discountLine.getDiscountDate() );
        setDiscountType( discountLine.getDiscountType() );
        setPurchase( RetailSystem.newInstance().getPurchase(
                discountLine.getPurchaseNumber() ) );
        setAmount( discountLine.getAmount() );
    }

    public void process()
    {
        adjustDiscount();

        aPurchase.setEndNbrOfDiscounts( aPurchase.getEndNbrOfDiscounts() + 1 );
        aPurchase.setEndTtlAmtOfDiscounts( aPurchase.getEndTtlAmtOfDiscounts()
                + amount );
    }

    public void adjustDiscount()
    {

    }

    public static Discount newDiscount( DiscountParser dp )
    {
        Discount aDiscount = null;

        switch ( dp.getDiscountType() )
        {
            case 'S':
            {
                aDiscount = new StoreDiscount();
                break;
            }
            case 'O':
            {
                aDiscount = new SeniorDiscount();
                break;
            }
            case 'P':
            {
                aDiscount = new PriceErrorDiscount();
                break;
            }
            case 'F':
            {
                aDiscount = new FrequentPurchaseDiscount();
                break;
            }
            case 'D':
            {
                aDiscount = new FloorItemDiscount();
                break;
            }
            case 'C':
            {
                aDiscount = new CouponDiscount();
                break;
            }
            case 'M':
            {
                aDiscount = new ManufacturerDiscount();
                break;
            }
            default:
            {
                System.out.println( "Invalid discount type '"
                        + dp.getDiscountType() + "' for purchase #:"
                        + dp.getPurchaseNumber() );
            }
        }

        return aDiscount;
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount( double amount )
    {
        if ( amount < 0 )
        {
            amount = 0;
        }

        this.amount = amount;
    }

    public Date getDiscountDate()
    {
        return discountDate;
    }

    public void setDiscountDate( Date discountDate )
    {
        this.discountDate = discountDate;
    }

    public char getDiscountType()
    {
        return discountType;
    }

    public void setDiscountType( char discountType )
    {
        this.discountType = discountType;
    }

    public Purchase getPurchase()
    {
        return aPurchase;
    }

    public void setPurchase( Purchase purchase )
    {
        this.aPurchase = purchase;
    }

}
