public class FrequentPurchaseDiscount extends Discount
{
    public final int MAX_FREQUENT_DISCOUNT = 25;

    public void adjustDiscount()
    {
        if ( getAmount() > MAX_FREQUENT_DISCOUNT );
        {
            setAmount( MAX_FREQUENT_DISCOUNT );
        }
    }
}
