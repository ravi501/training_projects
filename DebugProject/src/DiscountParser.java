import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DiscountParser
{
    String discountLine = null;

    public DiscountParser( String fileLine )
    {
        this.discountLine = fileLine;
    }

    public char getDiscountType()
    {
        return discountLine.substring( 15 ).charAt( 0 );
    }

    public String getPurchaseNumber()
    {
        return discountLine.substring( 0, 15 );
    }

    public Date getDiscountDate()
    {
        Date result = null;
        DateFormat df = new SimpleDateFormat( "yyyy-MM-dd" );

        try
        {
            result = df.parse( discountLine.substring( 16, 26 ) );
        }
        catch ( ParseException e )
        {
            e.printStackTrace();
        }

        return result;
    }

    public double getAmount()
    {
        double discountAmt = 0.0;

        try
        {
            discountAmt = (Double
                    .parseDouble( discountLine.substring( 26, 37 ) ) / 100);
        }
        catch ( ArithmeticException e )
        {
            System.out.println( "ERROR OCCURRED PARSING discountAmt" );
            e.printStackTrace();
        }

        return discountAmt;
    }

}
