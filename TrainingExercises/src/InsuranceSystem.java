import java.util.ArrayList;

/* Class Name:          Insurance System
   Language:            Java
   Author:              IS Training
   Date Written:        07/08/2015
   Application Area:    IS Training */

public class InsuranceSystem {
    
    private FileUtility policyFile = new FileUtility();
    private String policyFileName = "policy.txt";
    
    private FileUtility claimFile = new FileUtility();
    private String claimFileName = "claim.txt";
    
    private String policy;
    private String claim;
    private String holdPolicyNbr = " ";
    private String phoneNumber = "816.843.9155";
    
    private int nbrtransactions = 5;
    private int hailDeductible = -500;
    private int begTtlNbrOfClaims;
    private int endTtlNbrOfClaims;
    
    private double adjClaimAmt;
    private double begTtlAmtOfClaims;
    private double endTtlAmtOfClaims;
    private double begAvgAmtPerClaim;
    private double endAvgAmtPerClaim;
    private double claimAmt = 0.0;
    
    private final static int MAX_HAIL_CLAIM = 5000;
    private final static int MAX_EARTHQUAKE_CLAIM = 2500;
    private final static int MAX_BURGLARY_CLAIM = 10000;
    private final static int MAX_ACCIDENT_CLAIM = 2500;
    final static int HAIL_DEDUCTIBLE = 1000;
    final static int TORNADO_DEDUCTIBLE = 2500;
    final static int EARTHQUAKE_DEDUCTIBLE = 1000;
    final static int FIRE_DEDUCTIBLE = 1000;
    final static int FLOOD_DEDUCTIBLE = 2500;
    final static int BURGLARY_DEDUCTIBLE = 500;
    
    private ArrayList<String> policyArray = new ArrayList<String>();
    
    
    public InsuranceSystem(){
        System.out.println("InsuranceSystem is starting!");
    }
    
    public void run(){
        startUp();
        
        for (int i = 0; i < policyArray.size(); i++){
            processPolicy(i);
        }
        
        wrapUp();
    }
    
    private void startUp(){
        policyFile.openReadFile(policyFileName);
        claimFile.openReadFile(claimFileName);
        loadPolicyArray();
        claim = claimFile.getNextLine();
    }
    
    private void loadPolicyArray(){
        policy = policyFile.getNextLine();
        while ( policy != null){
            policyArray.add( policy );
            policy = policyFile.getNextLine();
        }
    }
    
    private void processPolicy(int x){
        policy = policyArray.get(x);
        holdPolicyNbr = getPolicyNbr( policy );
        
        begTtlNbrOfClaims = getNbrOfClaims( policy );
        endTtlNbrOfClaims = begTtlNbrOfClaims;
        begTtlAmtOfClaims = getAmtOfClaims( policy );
        endTtlAmtOfClaims = begTtlAmtOfClaims;
        
        while (( claim != null ) && ( getPolicyNbr( claim ).equals( holdPolicyNbr ) ) )
        {
            processClaims();
        }
        calcAndDisplayClaimTotals();
    }
    
    private void processClaims(){
        if(adjustClaim()){
            endTtlNbrOfClaims++;
            endTtlAmtOfClaims += adjClaimAmt;
        }
        claim = claimFile.getNextLine();
    }
    
    private boolean adjustClaim(){
        boolean validClaimType = true;
        adjClaimAmt = getClaimAmt( claim );
        
        switch ( getClaimType( claim ) ){
            case 'H': { //Hail
                if ( adjClaimAmt > MAX_HAIL_CLAIM ){
                    adjClaimAmt = MAX_HAIL_CLAIM;
                }
                adjClaimAmt -= HAIL_DEDUCTIBLE;
                break;
            }
            default: {
                System.out.println("For policy " + holdPolicyNbr + " , invalid claim type = " + getClaimType( claim ) );
                validClaimType = false;
            }
        }
        
        if ( adjClaimAmt < 0 ){
            adjClaimAmt = 0;
        }
        return validClaimType;
    }
    
    private void calcAndDisplayClaimTotals(){
        if ( begTtlNbrOfClaims > 0){
            begAvgAmtPerClaim = begTtlAmtOfClaims / begTtlNbrOfClaims;
        }
        else{
            begAvgAmtPerClaim = 0;
        }
        
        if ( endTtlNbrOfClaims > 0){
            endAvgAmtPerClaim = endTtlAmtOfClaims / endTtlNbrOfClaims;
        }
        else{
            endAvgAmtPerClaim = 0;
        }
        
        System.out.println("Policy#:    " + holdPolicyNbr + ", beginning average per amount = " + begAvgAmtPerClaim + ", ending average per amount = " + endAvgAmtPerClaim );
    }
    
    private void wrapUp(){
       System.out.println("InsuranceSystem is ending!"); 
    }
    
    private String getPolicyNbr(String policyLine) {
        return policyLine.substring(0, 15);
    }

    private int getNbrOfClaims(String policyLine) {
        int nbrOfClaims = 0;

        try {
            nbrOfClaims = Integer.parseInt(policyLine.substring(15, 17));
        } catch (ArithmeticException e) {
            System.out.println("ERROR OCCURRED PARSING nbrOfClaims");
            e.printStackTrace();
        }

        return nbrOfClaims;
    }

    private double getAmtOfClaims(String policyLine) {
        double amtOfClaims = 0.0;

        try {
            amtOfClaims = (Double.parseDouble(policyLine.substring(17, 28)) / 100);
        } catch (ArithmeticException e) {
            System.out.println("ERROR OCCURRED PARSING amtOfClaims");
            e.printStackTrace();
        }

        return amtOfClaims;
    }

    private char getClaimType(String claimLine) {
        return claimLine.substring(15).charAt(0);
    }

    private String getClaimDate(String claimLine) {
        return claimLine.substring(16, 26);
    }

    private double getClaimAmt(String claimLine) {
        double claimAmt = 0.0;

        try {
            claimAmt = (Double.parseDouble(claimLine.substring(26, 37)) / 100);
        } catch (ArithmeticException e) {
            System.out.println("ERROR OCCURRED PARSING claimAmt");
            e.printStackTrace();
        }

        return claimAmt;
    }

}