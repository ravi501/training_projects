public class MultiCorrectQuestion extends Question
{

    public void process()
    {
        // Stores the process method for multi correct answer question

        for ( int i = 0; i < getStudentAnswersArray().size(); i++ )
        {
            int totalCorrect = getCorrectOptionsArray().size();
            boolean studentAnswerCorrect = false;

            for ( int j = 0; j < getCorrectOptionsArray().size(); j++ )
            {

                if ( getStudentAnswersArray().get( i ).equals(
                        getCorrectOptionsArray().get( j ) ) )
                {
                    studentAnswerCorrect = true;
                    break;
                }
            }

            if ( studentAnswerCorrect )
            {
                setPointsAwarded( getPointsAwarded()
                        + (getPointsPossible() / totalCorrect) );
            }
            else
            {
                setPointsAwarded( getPointsAwarded()
                        - ((getPointsPossible() / totalCorrect)) );
            }

        }

    }
}
