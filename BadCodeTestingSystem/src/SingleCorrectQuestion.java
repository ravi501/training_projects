public class SingleCorrectQuestion extends Question
{
    // Stores the process method for the True False and Single Correct Answer
    // type questions
    public void process()
    {
        if ( getStudentAnswersArray().get( 0 ).equals( getCorrectOptionsArray().get( 0 ) ) )
        {
            setPointsAwarded( getPointsPossible() );
        }
    }
}
