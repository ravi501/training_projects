import java.util.ArrayList;


public abstract class Question
{
    // Question data is public because Test needs it to populate it from
    // TestData

    // Stores the type of the question
    private String              questionType;

    // Stores the total points possible for the question
    private int                 pointsPossible;

    // Stores the question asked
    private String              questionText;

    // Stores the list of options for the question
    private ArrayList< String > optionsArray          = new ArrayList< String >();

    // Stores the list of correct options
    private ArrayList< String > correctOptionsArray   = new ArrayList< String >();

    // Stores the list of student answers
    private ArrayList< String > studentAnswersArray   = new ArrayList< String >();

    // Stores the points awarded to the student
    private int                 pointsAwarded;

    // Constants for storing the index
    final static int            TEST_ID_INDEX         = 0;
    final static int            QUESTION_ID_INDEX     = 1;
    final static int            QUESTION_TYPE_INDEX   = 2;
    final static int            QUESTION_TEXT_INDEX   = 3;
    final static int            POINTS_POSSIBLE_INDEX = 4;

    // Constants for storing the type of question being asked
    final static String         TRUE_FALSE            = "True-False";
    final static String         SINGLE_CORRECT        = "Single-Correct";
    final static String         MULTI_CORRECT         = "Multi-Correct";
    final static String         SHORT_ANSWER          = "Short-Answer";
    final static String         LONG_ANSWER           = "Long-Answer";

    public abstract void process();

    public String getQuestionType()
    {
        return questionType;
    }

    public void setQuestionType( String questionType )
    {
        this.questionType = questionType;
    }

    public int getPointsPossible()
    {
        return pointsPossible;
    }

    public void setPointsPossible( int pointsPossible )
    {
        this.pointsPossible = pointsPossible;
    }

    public String getQuestionText()
    {
        return questionText;
    }

    public void setQuestionText( String questionText )
    {
        this.questionText = questionText;
    }

    public ArrayList< String > getOptionsArray()
    {
        return optionsArray;
    }

    public void setOptionsArray( ArrayList< String > optionsArray )
    {
        this.optionsArray = optionsArray;
    }

    public ArrayList< String > getCorrectOptionsArray()
    {
        return correctOptionsArray;
    }

    public void setCorrectOptionsArray( ArrayList< String > correctOptions )
    {
        this.correctOptionsArray = correctOptions;
    }

    public ArrayList< String > getStudentAnswersArray()
    {
        return studentAnswersArray;
    }

    public void setStudentAnswersArray( ArrayList< String > studentAnswers )
    {
        this.studentAnswersArray = studentAnswers;
    }

    public int getPointsAwarded()
    {
        return pointsAwarded;
    }

    public void setPointsAwarded( int pointsAwarded )
    {
        this.pointsAwarded = pointsAwarded;
    }

    // Builds each question
    // Because the Question data is public, we can refer to it directly here

    /**
     * Builds the question along with the options, correct options and student
     * answers
     * 
     * @param index
     *            - index for the ith question in the array
     */
    public void buildQuestion( int index )
    {
        String buildQuestion[] = TestData.myQuestions[ index ];
        questionType = buildQuestion[ QUESTION_TYPE_INDEX ];
        questionText = buildQuestion[ QUESTION_TEXT_INDEX ];
        pointsPossible = Integer
                .parseInt( buildQuestion[ POINTS_POSSIBLE_INDEX ] );

        buildOptions( index );
        buildCorrectOptions( index );
        buildStudentAnswers( index );
    }

    /**
     * Processes the question
     */
    public void processQuestion()
    {
        if ( getStudentAnswersArray().size() > 0 )
        {
            process();
            printSuccessMessage( getQuestionType() );
        }
        else
        {
            printFailureMessage( getQuestionType() );
        }
    }

    /**
     * Builds the options associated to the question
     * 
     * @param index
     */
    public void buildOptions( int index )
    {
        for ( int i = 0; i < TestData.myOptions.length; i++ )
        {
            optionsArray.add( TestData.myOptions[ index ][ i ] );
        }
    }

    /**
     * Builds the correct options for the question
     * 
     * @param index
     */
    public void buildCorrectOptions( int index )
    {
        for ( int i = 0; i < TestData.correctOptions.length; i++ )
        {
            correctOptionsArray.add( TestData.correctOptions[ index ][ i ] );
        }
    }

    /**
     * Build the student answer for the questions
     * 
     * @param index
     */
    public void buildStudentAnswers( int index )
    {
        for ( int i = 0; i < TestData.myStudentAnswers.length; i++ )
        {
            studentAnswersArray.add( TestData.myStudentAnswers[ index ][ i ] );
        }
    }

    /**
     * Prints the message for successful transaction
     * 
     * @param transactionType
     */
    public void printSuccessMessage( String transactionType )
    {
        System.out.println( "Finished processing a " + transactionType
                + " question" );
    }

    /**
     * Prints the message if the transaction is unsuccessful
     * 
     * @param transactionType
     */
    public void printFailureMessage( String transactionType )
    {
        System.out
                .println( "Could not grade a " + transactionType + " message" );
    }

    // Performs the following functions...
    // 1) Determines type of question
    // 2) Determines if studentAnswer matches correctOption
    // 3) Based on type of question, uses the student's answer to add all, some,
    // or none of the pointsPossible to pointsAwarded
    // 4) Prints out a message to the console on whether the
    /**
     * Method to process the student answers and assign points to the student
     * 
     * @return
     */
    public static Question getType( int i )
    {
        // True-false questions and single-correct questions are
        // exclusive-choice questions; only a single choice is selected as
        // the answer by the student.
        Question question = null;
        String type = TestData.myQuestions[ i ][ 2 ];

        // Processes the true-false questions
        if ( TRUE_FALSE.equals( type ) || SINGLE_CORRECT.equals( type ) )
        {
            question = new SingleCorrectQuestion();
        }

        // Processes the Multi-Correct questions
        else if ( MULTI_CORRECT.equals( type ) )
        { // Multi-Correct (aka multi-mark)
          // questions have many correct
          // answers

            question = new MultiCorrectQuestion();
        }

        // Processes the Short Answer questions
        else if ( SHORT_ANSWER.equals( type ) )
        {
            question = new ShortAnswerQuestion();
        }

        // Processes the Long Answer questions
        else if ( LONG_ANSWER.equals( type ) )
        {
            // Instructor must hand-grade the answer to this question type
            question = new LongAnswerQuestion();
        }
        else
        {
            System.out.println( "This is a bad question type: " + type );
        }
        return question;
    }
}
