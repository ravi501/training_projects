import java.util.ArrayList;


public class TestingSystem
{
    // TestingSystem class initializes the system

    // Stores the reference of the TestingSystem class
    private static TestingSystem instance;

    // Stores the array list of tests present in the system
    private ArrayList< Test >    testArray = new ArrayList< Test >();

    // Private constructor to TestingSystem class
    private TestingSystem()
    {

    }

    // Singleton pattern. Get the instance of the TestingSystem
    public static TestingSystem getInstance()
    {
        if ( instance == null )
        {
            instance = new TestingSystem();

        }
        return instance;
    }

    // Test's buildTest() method actually builds the tests, including questions,
    // and then grades it. Then, here we add that test to our array of tests.
    /**
     * Begins the initialization of the Testing System
     */
    public void process()
    {
        Test myTest;
        for ( int i = 0; i < TestData.myTests.length; i++ )
        {
            myTest = new Test();
            myTest.buildTest( i );
            myTest.process();
            testArray.add( myTest );
        }
    }
}
