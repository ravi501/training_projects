public class ShortAnswerQuestion extends Question
{

    @Override
    public void process()
    {
        // Stores the process functionality for short answer question
        for ( int i = 0; i < getStudentAnswersArray().size(); i++ )
        {
            if ( getStudentAnswersArray().get( 0 ).equals(
                    getCorrectOptionsArray().get( i ) ) )
            {
                setPointsAwarded( getPointsPossible() );
                break;
            }
        }

    }

}
