public class TestData {
	public static String[][] myTests = {
			// Sample test: {"Test ID", "Course Name", "Code", "RoomNbr",
			// "Instructor(s)"},
			{ "Test1", "How to Write Bad Code", "IST 37870", "TH316",
					"Derek and Aaron" },
			{ "Test2", "Java Fundamentals", "IST 37500", "TH333", "Aaron" } };

	public static String[][] myQuestions = {
			// Sample question: {"Test ID", "Question ID", "Question Type",
			// "Question Text", "Points Possible"}
			{
					"Test1",
					"01",
					"Multi-Correct",
					"Which of the following are general rules of thumb from this course?",
					"3" },
			{ "Test1", "02", "True-False",
					"Every method should have one and only one function.", "1" },
			{ "Test2", "01", "Single-Correct",
					"Identifying bad code is important because...", "1" },
			{
					"Test2",
					"02",
					"Short Answer",
					"______________ could be defined as the process of turning bad code into good code.",
					"3" } };

	public static String[][] myOptions = {
			// Sample answer: {"Test ID", "Question ID", Correct Options (in an
			// array)}
			{ "Test1", "01", "Methods should be simple" },
			{ "Test1", "01", "Methods should be short" },
			{ "Test1", "01",
					"Use one concatenated statement if it's easy to understand" },
			{
					"Test1",
					"01",
					"Always use try/catch blocks around any code that may return a null pointer exception" },
			{ "Test1", "02", "True" }, 
			{ "Test1", "02", "False" } };

	public static String[][] correctOptions = {
			// Sample answer: {"Test ID", "Question ID", Correct Options (may be
			// multiple if multi-correct question type)}
			{ "Test1", "01", "Methods should be simple" },
			{ "Test1", "01", "Methods should be short" },
			{ "Test1", "01",
					"Use one concatenated statement if it's easy to understand" },
			{ "Test1", "02", "True" }, };

	public static String[][] myStudentAnswers = {
			// Sample answer: {"Student ID", "Test ID", "Question ID", Answers
			// (may be multiple if multi-correct question type)
			{ "06025", "Test1", "01", "Methods should be simple" },
			{ "06025", "Test1", "01", "Methods should be short" },
			{
					"06025",
					"Test1",
					"01",
					"Always use try/catch blocks around any code that may return a null pointer exception" },
			{ "06025", "Test1", "02", "True" }, };

}
