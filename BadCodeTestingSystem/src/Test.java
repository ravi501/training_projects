import java.util.ArrayList;


public class Test
{

    // Stores the test ID
    private String                testID;

    // Stores the course name
    private String                courseName;

    // Stores the course code
    private String                courseCode;

    // Stores the room number in which the course takes place
    private String                roomNumber;

    // Question array must be public because ComputerBasedTest and
    // InternetBasedTest need it
    private ArrayList< Question > questionArray     = new ArrayList< Question >();

    final static int              TEST_ID_INDEX     = 0;
    final static int              COURSE_NAME_INDEX = 1;
    final static int              COURSE_CODE_INDEX = 2;
    final static int              ROOM_NUMBER_INDEX = 3;
    final static int              INSTRUCTOR_INDEX  = 4;

    public String getTestID()
    {
        return testID;
    }

    public void setTestID( String testID )
    {
        this.testID = testID;
    }

    public String getCourseName()
    {
        return courseName;
    }

    public void setCourseName( String courseName )
    {
        this.courseName = courseName;
    }

    public String getCourseCode()
    {
        return courseCode;
    }

    public void setCourseCode( String courseCode )
    {
        this.courseCode = courseCode;
    }

    public String getRoomNumber()
    {
        return roomNumber;
    }

    public void setRoomNumber( String roomNumber )
    {
        this.roomNumber = roomNumber;
    }

    public ArrayList< Question > getQuestionArray()
    {
        return questionArray;
    }

    public void setQuestionArray( ArrayList< Question > questionArray )
    {
        this.questionArray = questionArray;
    }

    // The following method has these functions...
    // 1) Builds each question in the question array
    // 2) Grades each question using the proc() method
    /**
     * Builds the test objects
     * 
     * @param i
     */
    public void buildTest( int i )
    {
        String testData[] = TestData.myTests[ i ];

        setTestID( testData[ TEST_ID_INDEX ] );
        setCourseName( testData[ COURSE_NAME_INDEX ] );
        setCourseCode( testData[ COURSE_CODE_INDEX ] );
        setRoomNumber( testData[ ROOM_NUMBER_INDEX ] );

        buildQuestionArray();
    }

    /**
     * Processes the student answers in the test
     */
    public void process()
    {
        for ( int i = 0; i < getQuestionArray().size(); i++ )
        {
            getQuestionArray().get( i ).processQuestion();
        }
    }

    // Build the question array
    /**
     * Builds the question array
     */
    private void buildQuestionArray()
    {
        for ( int i = 0; i < TestData.myQuestions.length; i++ )
        {
            Question myQuestion = Question.getType( i );
            myQuestion.buildQuestion( i );
            getQuestionArray().add( myQuestion );
        }
    }
}
