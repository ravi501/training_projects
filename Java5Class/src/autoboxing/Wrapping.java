
package autoboxing;

public class Wrapping
{

    public static void main( String[] args )
    {
        // Does 127 == 127 and 128 == 128?
        // Or 127 equals 127 and 128 equals 128?
        System.out.println( "== | Value of 127: "
                + (Integer.valueOf( 127 ) == Integer.valueOf( 127 )) );
        System.out.println( "== | Value of 128: "
                + (Integer.valueOf( 128 ) == Integer.valueOf( 128 )) );

        System.out.println( ".Equals | Value of 127: "
                + Integer.valueOf( 127 ).equals( Integer.valueOf( 127 ) ) );
        System.out.println( ".Equals | Value of 128: "
                + Integer.valueOf( 128 ).equals( Integer.valueOf( 128 ) ) );

        System.out.println( "== | .intValue of 128: "
                + (new Integer( 128 ).intValue() == new Integer( 128 )
                        .intValue()) );
        System.out.println( "== | parseInt 128: "
                + (Integer.parseInt( "128" ) == Integer.parseInt( "128" )) );

        // NOTES:
        // Remember that �==� is always used for object equality,
        // it has not been overloaded for comparing unboxed values.
        //
        // From Java Language Specification section 5.1.7
        // -
        // http://java.sun.com/docs/books/jls/third_edition/html/conversions.html#5.1.7
        // - If the value p being boxed is true, false, a byte,
        // a char in the range \u0000 to \u007f, or an int or
        // short number between -128 and 127,
        // then let r1 and r2 be the results of any two boxing conversions of p.
        // It is always the case that r1 == r2.

    }
}
