
package fundamentals;

class OuterClass2
{
    private static int classVariable = 100;

    private static void classMethod()
    {
        System.out.println( "OuterClass2       -> classMethod()" );
    }

    private int instanceVariable = 200;

    private void instanceMethod()
    {
        System.out.println( "OuterClass2       -> instanceMethod()" );
    }

    private void anotherInstanceMethod()
    {
        System.out.println( "OuterClass2       -> anotherInstanceMethod()" );
    }

    private InnerClass myInner = new InnerClass();

    public InnerClass getMyInner()
    {
        return myInner;
    }

    // Has access to *ALL* members of Outer class
    public class InnerClass
    {

        public void instanceMethod()
        {
            System.out.println( "InnerClass        -> instanceMethod()" );
            classMethod();
            classVariable = 0;
            // Without outer class reference, you will get a stack overflow
            OuterClass2.this.instanceMethod();
            anotherInstanceMethod();
            instanceVariable = 0;
        }

        // Because an inner class is associated with an instance, it cannot
        // define any static members itself.
        // public static void classMethod() {}
    }
}


public class InnerClassExample1
{

    public static void main( String[] args )
    {
        // Create a new instance of the outer class
        OuterClass2 outer = new OuterClass2();

        // Create a new instance of the inner class
        OuterClass2.InnerClass nestedObject1 = outer.getMyInner();
        nestedObject1.instanceMethod();

        // Alternate way to create a new instance of the inner class
        OuterClass2.InnerClass nestedObject2 = new OuterClass2().new InnerClass();
    }
}

/*
InnerClass        -> instanceMethod()
OuterClass2       -> classMethod()
OuterClass2       -> instanceMethod()
OuterClass2       -> anotherInstanceMethod()
 */