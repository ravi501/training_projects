
package fundamentals;

class Animal
{
    @SuppressWarnings( "unused" )
    private void instanceMethod()
    {
    }

    protected void anotherInstanceMethod()
    {
    }

    public static void classMethod()
    {
    }

    @Deprecated
    public static void deprecatedMethod()
    {
    }
}


class Horse extends Animal
{
    // Adding @Override will cause compile errors
    public void instanceMethod()
    {
    }

    @Override
    public void anotherInstanceMethod()
    {
    }

    // Adding @Override will cause compile errors
    public static void classMethod()
    {
    }
}


public class AnnotationsExample1
{

    @SuppressWarnings( { "static-access", "unused" } )
    public static void main( String[] args )
    {
        // Calling a deprecated method
        Animal.deprecatedMethod();

        Horse horse1 = new Horse();

        // Without the annotation to suppress warnings for "static-access",
        // accessing a class method using an instance would generate a warning
        horse1.classMethod();

        // Without the annotation to suppress warnings for "unused", the
        // following line will generate a warning
        Horse horse2 = new Horse();
    }
}


// http://mindprod.com/jgloss/annotations.html#SUPPRESSWARNINGS