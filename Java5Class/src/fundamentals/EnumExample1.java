
package fundamentals;


import java.io.Serializable;


public class EnumExample1
{
    // enums can implement interfaces
    public enum CoffeeCup implements Serializable
    {
        SMALL( 8, "Tall" ), MEDIUM( 16, "Grande" ), LARGE( 24, "Venti" );

        // Declaration
        private int    ounces;
        private String title;

        // Constructor must be package-private or private
        CoffeeCup( int ounces, String title )
        {
            this.ounces = ounces;
            this.title = title;
        }

        public int getOunces()
        {
            return this.ounces;
        }

        public String getTitle()
        {
            return this.title;
        }
    }

    public static void main( String[] args )
    {
        for ( CoffeeCup cup : CoffeeCup.values() )
        {
            System.out.format( "%s = %d ounces (%s)%n", cup, cup.getOunces(),
                    cup.getTitle() );
        }
    }
}

/*
SMALL = 8 ounces (Tall)
MEDIUM = 16 ounces (Grande)
LARGE = 24 ounces (Venti) 
 */