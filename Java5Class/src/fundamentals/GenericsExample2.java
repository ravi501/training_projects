
package fundamentals;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


class LegacyCode
{
    // Return the sum
    public static int getTotal( List list )
    {
        Iterator it = list.iterator();
        int total = 0;
        while ( it.hasNext() )
        {
            int i = (( Integer ) it.next()).intValue();
            total += i;
        }
        return total;
    }

    // Insert a string into the list
    public static void insert( List list )
    {
        list.add( "hello" );
    }
}


public class GenericsExample2
{
    public static void main( String[] args )
    {
        List< Integer > myList = new ArrayList< Integer >();
        myList.add( 10 );
        myList.add( 20 );
        System.out.format( "Sum of items: %d%n", LegacyCode.getTotal( myList ) );

        // The following code will not compile
        // myList.add("hello");

        // The following code will compile and work fine
        LegacyCode.insert( myList );
        
        System.out.format( "Count of items: %d%n", myList.size() );
        for ( Object obj : myList )
        {
            System.out.print( obj + " " );
        }
        
        System.out.format( "Sum of items: %d%n", LegacyCode.getTotal( myList ) );
    }
}


/*
Sum of items: 30
Count of items: 3
10 20 hello 
*/