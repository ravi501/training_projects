
package fundamentals;

public class InitBlocksExample1
{
    public static void main( String[] args )
    {
        new Toyota(); // first instance
        System.out.println( "" );
        new Toyota(); // second instance
    }
}


class Car
{
    static
    {
        System.out.println( "Car    -> Static block 1" );
    }

    {
        System.out.println( "Car    -> Instance block 1" );
    }

    Car()
    {
        System.out.println( "Car    -> Constructor" );
    }

    static
    {
        System.out.println( "Car    -> Static block 2" );
    }

    {
        System.out.println( "Car    -> Instance block 2" );
    }
}


class Toyota extends Car
{
    public Toyota()
    {
        System.out.println( "Toyota -> Constructor" );
    }

    static
    {
        System.out.println( "Toyota -> Static block 1" );
    }

    {
        System.out.println( "Toyota -> Instance block 1" );
    }
}

/*
Car    -> Static block 1
Car    -> Static block 2
Toyota -> Static block 1
Car    -> Instance block 1
Car    -> Instance block 2
Car    -> Constructor
Toyota -> Instance block 1
Toyota -> Constructor

Car    -> Instance block 1
Car    -> Instance block 2
Car    -> Constructor
Toyota -> Instance block 1
Toyota -> Constructor
*/