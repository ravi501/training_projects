
package fundamentals;

public class EnhancedForExample1
{

    public static void main( String[] args )
    {
        int[] numbers = { 1, 2, 3 };
        for ( int item : numbers )
        {
            System.out.println( "Count is: " + item );
        }

        java.util.ArrayList< Object > list = new java.util.ArrayList< Object >();
        list.add( 123 );
        list.add( "Hello, World!" );
        list.add( new java.util.Date() );
        for ( Object obj : list )
        {
            System.out.println( obj.toString() );
        }
    }
}

/*
 * Count is: 1
 * Count is: 2
 * Count is: 3
 * 123
 * Hello, World!
 * Mon Feb 11 08:25:58 CST 2008
 */
