
package fundamentals;


import java.util.Currency;
import java.util.Locale;


public class VarargsExample1
{

    // method with one var-arg argument
    private static void printNumbers( int... integers )
    {
        if ( integers != null )
        {
            System.out.format( "%d parameters:  ", integers.length );
            for ( int i : integers )
            {
                System.out.format( "%s ", i );
            }
            System.out.println( "" );
        }
    }

    // method with multiple arguments including a var-arg
    private static void printStuff( int num, char ch, Currency... currencies )
    {
        System.out.format( "int=%d, char=%s, ", num, ch );
        if ( currencies != null )
        {
            System.out.format( "%d parameters:  ", currencies.length );
            for ( int i = 0; i < currencies.length; i++ )
            {
                System.out.format( "%s ", currencies[ i ] );
            }
        }
        System.out.println( "" );
    }

    public static void main( String[] args )
    {
        // calling method with no parameters
        printNumbers();

        // calling method with 3 parameters
        printNumbers( 1, 2, 3 );

        // calling method with an array
        printNumbers( new int[] { 100, 200, 300, 400, 500 } );

        // calling method with null
        printNumbers( null );

        Currency dollar = Currency.getInstance( Locale.US );
        Currency pound = Currency.getInstance( Locale.UK );
        // calling method with multiple parameters including 2 currencies
        printStuff( 99, 'X', dollar, pound );
    }
}

/*
0 parameters:  
3 parameters:  1 2 3 
5 parameters:  100 200 300 400 500 
int=99, char=X, 2 parameters:  USD GBP 
*/