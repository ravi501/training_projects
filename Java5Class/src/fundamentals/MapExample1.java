
package fundamentals;


import java.util.HashMap;
import java.util.Map;


/*
 * When using a class that implements Map, any classes that you use as part of
 * the keys for that map should override the hashCode() and equals() methods.
 */
class Dog
{
    public String name;

    public Dog( String n )
    {
        name = n;
    }

    public boolean equals( Object o )
    {
        return ((o instanceof Dog) && ((( Dog ) o).name == name));
    }

    public int hashCode()
    {
        return name.length(); // simple implementation
    }
}


/*
 * Without an explicit override of hashCode() or equals(), this class will
 * return a unique hashCode for each instance.
 */
class Cat
{
}


/*
 * java.lang.Enum overrides hashCode() and equals()
 */
enum Pets
{
    DOG, CAT, HORSE
}


public class MapExample1
{

    public static void main( String[] args )
    {
        Map< Object, Object > map = new HashMap< Object, Object >();

        map.put( "k1", new Dog( "aiko" ) ); // #1
        System.out.println( "#1 " + map.get( "k1" ) );

        map.put( "k2", Pets.DOG ); // #2
        String k2 = "k2";
        System.out.println( "#2 " + map.get( k2 ) );

        map.put( Pets.CAT, "CAT key" ); // #3
        Pets p = Pets.CAT;
        System.out.println( "#3 " + map.get( p ) );

        Dog myDog = new Dog( "clover" ); // #4
        map.put( myDog, "Dog key" );
        System.out.println( "#4 " + map.get( myDog ) );

        map.put( new Cat(), "Cat key" ); // #5
        System.out.println( "#5 " + map.get( new Cat() ) );

        System.out.println( "Size = " + map.size() );

        myDog.name = "magnolia";
        System.out.println( "#6 " + map.get( myDog ) ); // #6

        myDog.name = "clover";
        System.out.println( "#7 " + map.get( new Dog( "clover" ) ) ); // #7

        myDog.name = "arthur";
        System.out.println( "#8 " + map.get( new Dog( "clover" ) ) ); // #8
    }
}

/*
#1 Dog@4
#2 DOG
#3 CAT key
#4 Dog key
#5 null
Size = 5
#6 null
#7 Dog key
#8 null
 */