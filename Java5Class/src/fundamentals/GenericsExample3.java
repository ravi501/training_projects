
package fundamentals;

class Box
{
    private Object object;

    public Box( Object object )
    {
        this.object = object;
    }

    public void replace( Object object )
    {
        this.object = object;
    }

    public Object get()
    {
        return object;
    }
}


class GenericBox< T >
{
    private T t; // T stands for "Type"

    public GenericBox( T t )
    {
        this.t = t;
    }

    public void replace( T t )
    {
        this.t = t;
    }

    public T get()
    {
        return t;
    }
}


class AnotherGenericBox< T, U >
{
    private T t;
    private U u;

    public AnotherGenericBox( T t, U u )
    {
        this.t = t;
        this.u = u;
    }
}


public class GenericsExample3
{
    public static void main( String[] args )
    {
        Box box = new Box( 10 );
        box.replace( new Integer( 20 ) );
        Integer x = ( Integer ) box.get();

        GenericBox< Integer > genericBox = new GenericBox< Integer >( 10 );
        genericBox.replace( new Integer( 20 ) );
        Integer y = genericBox.get();

        AnotherGenericBox< String, Boolean > anotherBox;
        anotherBox = new AnotherGenericBox< String, Boolean >( "abc", true );
    }
}
