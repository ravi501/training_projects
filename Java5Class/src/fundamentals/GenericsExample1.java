
package fundamentals;


import java.util.ArrayList;
import java.util.List;


public class GenericsExample1
{

    public static void main( String[] args )
    {

        // ********************************************************************
        // NON-GENERIC, LEGACY EXAMPLE
        // ********************************************************************
        // Create a list to hold integers
        List myIntList1 = new ArrayList();
        // Add an integer to the list
        myIntList1.add( 0, new Integer( 123 ) );
        // Retrieve the integer from the list
        Integer x1 = ( Integer ) myIntList1.get( 0 );

        // ********************************************************************
        // SAME EXAMPLE USING GENERICS
        // ********************************************************************
        // Create a list to hold integers (and only integers)
        List< Integer > myIntList2 = new ArrayList< Integer >();
        // Add an integer to the list
        myIntList2.add( 0, new Integer( 456 ) );
        // Retrieve the integer from the list
        Integer x2 = myIntList2.get( 0 );

        // ********************************************************************
        // DIFFERENCES
        // ********************************************************************
        // Non-generic list cannot prevent non-integers from being added
        myIntList1.add( 1, "Hello, World!" );
        // The following line will only result in a runtime error but
        // will compile fine
        x1 = ( Integer ) myIntList1.get( 1 );

        // However, a generic list will prevent such problems
        // The following line will not compile
        // myIntList2.add(1, "Hello, World!");
    }
}
