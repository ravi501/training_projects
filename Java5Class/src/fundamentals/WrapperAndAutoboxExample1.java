
package fundamentals;

public class WrapperAndAutoboxExample1
{

    public static void main( String[] args )
    {

        // Declaration
        int primitiveInt;
        Integer intObject;

        // Without Autoboxing (pre Java 5)
        primitiveInt = 1;
        intObject = new Integer( 2 );
        primitiveInt = intObject.intValue();
        intObject = new Integer( primitiveInt );

        // With Autoboxing
        primitiveInt = 1;
        intObject = 2;
        primitiveInt = intObject;
        intObject = primitiveInt;
    }
}
