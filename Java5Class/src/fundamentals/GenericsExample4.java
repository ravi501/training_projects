
package fundamentals;


import java.util.ArrayList;
import java.util.List;


public class GenericsExample4
{

    static Integer         myInt       = new Integer( 1 );
    static Float           myFloat     = new Float( 1.2f );
    static List< Number >  numberList  = new ArrayList< Number >();
    static List< Integer > integerList = new ArrayList< Integer >();
    static List< Float >   floatList   = new ArrayList< Float >();
    static List< Object >  objectList  = new ArrayList< Object >();
    static List< String >  stringList  = new ArrayList< String >();

    static
    {
        // We can add an int and a float here since Number is an abstract class
        numberList.add( myInt );
        numberList.add( myFloat );

        integerList.add( myInt );
        floatList.add( myFloat );
    }

    public static void main( String[] args )
    {

        // Can pass any subtype of Number to Number parameter
        doSomething( myInt );
        doSomething( myFloat );

        // Type of the variable declaration must match the parameter type
        doSomething( numberList );
        // The following lines will not compile
        // doSomething(integerList);
        // doSomething(floatList);

        upperBoundedType( numberList );
        upperBoundedType( integerList );
        upperBoundedType( floatList );

        upperBoundedWildcard( numberList );
        upperBoundedWildcard( integerList );
        upperBoundedWildcard( floatList );

        lowerBoundedWildcard( integerList );
        lowerBoundedWildcard( numberList );
        lowerBoundedWildcard( objectList );
        // The following line will not compile
        // doSomething4(floatList);

        unboundedWildcard( numberList );
        unboundedWildcard( integerList );
        unboundedWildcard( floatList );
        unboundedWildcard( objectList );
        unboundedWildcard( stringList );
    }

    // Simple type
    static void doSomething( Number number )
    {
    }

    // Generic type
    static void doSomething( List< Number > list )
    {
    }

    // Generic Bounded type parameter (upper bound)
    static < U extends Number > void upperBoundedType( List< U > list )
    {
    }

    // Generic Upper-bounded Wildcard
    static void upperBoundedWildcard( List< ? extends Number > list )
    {
    }

    // Generic Lower-bounded Wildcard
    static void lowerBoundedWildcard( List< ? super Integer > list )
    {
    }

    // Generic Unbounded Wildcard (NOT same as List<Object>)
    static void unboundedWildcard( List< ? > list )
    {
    }
}
