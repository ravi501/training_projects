/**
 *
 * COPYRIGHT:
 *
 *   The computer systems, procedures, data bases and
 *   programs created and maintained by DST Systems, Inc.,
 *   are proprietary in nature and as such are confidential.
 *   Any unauthorized use or disclosure of such information
 *   may result in civil liabilities.
 *
 *   Copyright 2000 by DST Systems, Inc.
 *
 *******************************************************************************
 */
package dst.util;



/**
 * A class containing various useful string functions that are not available
 * in the standard JDK.  These are methods that many find people find
 * themselves writing, so they are included here to promote reuse.
 *
 * @author  DST Systems, Inc.
 */

public class StrUtil extends Object
{


   /**
    * Tests all chars in a string to see if they are alphabetic. Uses
    * Character.isLetter().
    *
    * @param  aString the string to test
    * @return true if all are letters, false otherwise. An empty
    *         String returns true.
    */
   public static final boolean isAllAlpha( String aString )
   {
      if ( isEmpty(aString) )  return true;

      for ( int i = 0; i < aString.length(); i++ )
      {
         if ( false == Character.isLetter( aString.charAt( i ) ) )
         {
            return false;
         }
      }

      return true;
   }

   /**
    * Tests all chars in a string to see if they are digits. Uses isDigit
    * from the Character class.
    *
    * @param  aString the string to test
    * @return true if all are digits, false otherwise. An empty
    *         String returns true.
    */
   public static final boolean isAllDigits( String aString )
   {
      if ( isEmpty(aString) ) return true;

      for ( int i = 0; i < aString.length(); i++ )
      {
         if ( false == Character.isDigit( aString.charAt( i ) ) )
         {
            return false;
         }
      }

      return true;
   }

   /**
    * Tests the string to see if it is a decimal value. Uses isDigit from the
    * Character class. The first char can be a digit, '.', '+', '-', or a space.
    * All other chars must be digits or a '.'. There is only one '.' allowed.
    * This will also return true on integers.
    *
    * @param  aString the string to test
    * @return true if it is, false otherwise.  This will also return true on
    *         integers.
    */
   public static final boolean isDecimal( String aString )
   {
      if ( isEmpty(aString) ) return false;

      int digits = 0;
      int     strLen = aString.length();
      boolean hasDecimalPt = false;
      char first = aString.charAt( 0 );

      switch ( first )
      {
         case '.':
            hasDecimalPt = true;
            break;
         case '0':
         case '1':
         case '2':
         case '3':
         case '4':
         case '5':
         case '6':
         case '7':
         case '8':
         case '9':
         case '-':
         case '+':
         case ' ':
         case '\0':
            break;

         default:
            return false;
      }

      if ( Character.isDigit( first ) )
      {
         digits = 1;
      }

      for ( int count = 1;  count < strLen;  count++ )
      {
         char ch = aString.charAt( count );
         if ( false == Character.isDigit( ch ) )
         {
            if ( '.' == ch )
            {
               if ( true == hasDecimalPt )
               {
                  return false;       //cant have 2 decimal pts.
               }
               else
               {
                  hasDecimalPt = true;   //1st decimal pt
                  continue;
               }
            }
            else
            {
               return false;
            }
         }//end if false..
         ++digits;
      } //end for
      if ( digits > 0 )
      {
         return true;
      }
      return false;
   }



   /**
    * Pads a string on the left out to a certain number of chars
    * with any pad character.  If the string is longer than
    * padlength, then it will be truncated to the padlength.
    *
    * @param  aString   the string to pad
    * @param  padLength the new length of the string
    * @param  padChar   the char to pad with
    * @return the padded String
    */
   public static String padLeftTruncate( String inText,
                                         int padLength,
                                         char inPadChar )
   {
      int len = inText.length();
      if ( len > padLength )
      {
         //Must truncate
         return inText.substring( len - padLength ) ;
      }
      StringBuffer newTextBuff = new StringBuffer( inText );
      //Pad with inPadChar
      for ( int x = len; x < padLength; x++ )
      {
         newTextBuff.insert( 0, inPadChar );
      }
      return  newTextBuff.toString();
   }


   /**
    * Replace all occurrances of strRemove with strAdd in strSource.
    *
    * @param strSource - the source string
    * @param strRemove - the substring to replace
    * @param strAdd - the replacement substring
    * @return a new string
    */
   public static final String replaceAll( String strSource, String strRemove,
                                          String strAdd )
   {
      int iRemoveLen = strRemove.length();
      int iAddLen = strAdd.length();
      int iRemovePos = -1;

      while ( -1 != ( iRemovePos = strSource.indexOf( strRemove,
                                                      iRemovePos ) ) )
      {
         strSource = strSource.substring( 0, iRemovePos ) +
                     strAdd +
                     strSource.substring( iRemovePos + iRemoveLen );

         iRemovePos += iAddLen;
      }

      return strSource;
   }

   private static final boolean isEmpty(String aString)
   {
      return( (null == aString) || (0   == aString.length()) );
   }
}


