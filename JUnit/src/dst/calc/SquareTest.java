package dst.calc;
import org.junit.After;
import org.junit.Before;


import dst.calc.Calculator;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SquareTest {

    private static Calculator calculator = new Calculator();
    private int param;
    private int result;

    @Parameters
    public static Collection data() {
        return Arrays.asList(new Object[][]{
                {0, 0},
                {1, 1},
                {2, 4},
                {4, 16},		// OK : 4� = 16
                {5, 25},
                {6, 36},
                {7, 48}		// NOK : 7� = 49 not 48
        });
    }

    public SquareTest(int param, int result) {
        this.param = param;
        this.result = result;
    }

    @Test
    public void square() {
        calculator.square(param);
        assertEquals(result, calculator.getResult());
    }

}