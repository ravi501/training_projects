import org.junit.*;
import static org.junit.Assert.*;

public class ExampleTest {

	@Test
	public void testInvalid()
	{
		assertTrue( 1 == 2 );
	}

	@Test
	public void testMessage()
	{
		assertTrue( "This is a message", 1 + 1 == 2 );
	}
	
	@Test
	public void testFail()
	{
		fail("This test was written to fail");
	}
	
	@Test
	public void testException() throws Exception
	{
		throw new Exception();
	}
}
