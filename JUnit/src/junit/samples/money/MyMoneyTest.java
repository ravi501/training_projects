
package junit.samples.money;


import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class MyMoneyTest
{

    @Test
    public void testEmptyArray()
    {
        int[] intArray = new int[ 5 ];
        assertTrue( intArray.length == 0 );
    }

}
