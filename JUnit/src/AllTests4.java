import junit.framework.Test;
import junit.framework.TestSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({dst.calc.SquareTest.class, dst.calc.test.CalculatorTest.class, junit.samples.money.MoneyTest.class})
public class AllTests4 {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for moretests");
		//$JUnit-BEGIN$
		
		//$JUnit-END$
		return suite;
	}

}
